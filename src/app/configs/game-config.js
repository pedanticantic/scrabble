System.register(["./boards/standard-board", "./boards/super-board"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var standard_board_1, super_board_1, GAME_CONFIGS;
    return {
        setters: [
            function (standard_board_1_1) {
                standard_board_1 = standard_board_1_1;
            },
            function (super_board_1_1) {
                super_board_1 = super_board_1_1;
            }
        ],
        execute: function () {
            exports_1("GAME_CONFIGS", GAME_CONFIGS = {
                boards: [
                    standard_board_1.standardBoard,
                    super_board_1.superBoard
                ]
            });
        }
    };
});
//# sourceMappingURL=game-config.js.map