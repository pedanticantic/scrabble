// This defines the board and tile configs.
import {standardBoard} from "./boards/standard-board";
import {superBoard} from "./boards/super-board";

export const GAME_CONFIGS = {
    boards: [
        standardBoard,
        superBoard
    ]
};