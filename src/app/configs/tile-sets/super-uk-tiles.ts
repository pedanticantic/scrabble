import {LinguaDriver} from '../../services/word-lookup-drivers/LinguaDriver';

export const superUkTiles = {
    name: 'Super UK tile set',
    // The language and API.
    languageApiDriver: new LinguaDriver(),
    // Format is points value => array of letter & number of tiles.
/*
 4 blank tiles (scoring 0 points)
 1 point: E ×24, A ×16, O ×15, T ×15, I ×13, N ×13, R ×13, S ×10, L ×7, U ×7
 2 points: D ×8, G ×5
 3 points: C ×6, M ×6, B ×4, P ×4
 4 points: H ×5, F ×4, W ×4, Y ×4, V ×3
 5 points: K ×2
 8 points: J ×2, X ×2
 10 points: Q ×2, Z ×2
 */
    tiles: {
        '0': [
            [' ', 4]
        ],
        '1': [
            ['E', 24],
            ['A', 16],
            ['I', 13],
            ['O', 15],
            ['N', 13],
            ['R', 13],
            ['T', 15],
            ['L', 7],
            ['S', 10],
            ['U', 7]
        ],
        '2': [
            ['D', 8],
            ['G', 5]
        ],
        '3': [
            ['B', 4],
            ['C', 6],
            ['M', 6],
            ['P', 4]
        ],
        '4': [
            ['F', 4],
            ['H', 5],
            ['V', 3],
            ['W', 4],
            ['Y', 4]
        ],
        '5': [
            ['K', 2]
        ],
        '8': [
            ['J', 2],
            ['X', 2],
        ],
        '10': [
            ['Q', 2],
            ['Z', 2]
        ]
    }
};
