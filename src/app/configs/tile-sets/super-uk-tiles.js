System.register(["../../models/word-lookup.api.config"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var word_lookup_api_config_1, superUkTiles;
    return {
        setters: [
            function (word_lookup_api_config_1_1) {
                word_lookup_api_config_1 = word_lookup_api_config_1_1;
            }
        ],
        execute: function () {
            exports_1("superUkTiles", superUkTiles = {
                name: 'Super UK tile set',
                // The language and API.
                languageApiConfig: new word_lookup_api_config_1.WordLookupApiConfig('EN-world', 'http://api.wordnik.com/v4/word.json/{word}/definitions?limit=200&includeRelated=false&useCanonical=false&includeTags=false&sourceDictionaries=wiktionary&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5&callback=JSONP_CALLBACK', function response(result) {
                    console.log('Response callback being called - defined in the tileset');
                    return result.length > 0;
                }, function error(error) {
                    console.log('Error callback being called');
                    return false; // Just assume the word is not valid.
                }, 'Word validation API provided by api.wordnik.com'),
                // Format is points value => array of letter & number of tiles.
                /*
                 4 blank tiles (scoring 0 points)
                 1 point: E ×24, A ×16, O ×15, T ×15, I ×13, N ×13, R ×13, S ×10, L ×7, U ×7
                 2 points: D ×8, G ×5
                 3 points: C ×6, M ×6, B ×4, P ×4
                 4 points: H ×5, F ×4, W ×4, Y ×4, V ×3
                 5 points: K ×2
                 8 points: J ×2, X ×2
                 10 points: Q ×2, Z ×2
                 */
                tiles: {
                    '0': [
                        [' ', 4]
                    ],
                    '1': [
                        ['E', 24],
                        ['A', 16],
                        ['I', 13],
                        ['O', 15],
                        ['N', 13],
                        ['R', 13],
                        ['T', 15],
                        ['L', 7],
                        ['S', 10],
                        ['U', 7]
                    ],
                    '2': [
                        ['D', 8],
                        ['G', 5]
                    ],
                    '3': [
                        ['B', 4],
                        ['C', 6],
                        ['M', 6],
                        ['P', 4]
                    ],
                    '4': [
                        ['F', 4],
                        ['H', 5],
                        ['V', 3],
                        ['W', 4],
                        ['Y', 4]
                    ],
                    '5': [
                        ['K', 2]
                    ],
                    '8': [
                        ['J', 2],
                        ['X', 2],
                    ],
                    '10': [
                        ['Q', 2],
                        ['Z', 2]
                    ]
                }
            });
        }
    };
});
//# sourceMappingURL=super-uk-tiles.js.map