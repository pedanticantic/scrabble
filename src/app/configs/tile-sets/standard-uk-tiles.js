System.register(["../../models/word-lookup.api.config"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var word_lookup_api_config_1, standardUkTiles;
    return {
        setters: [
            function (word_lookup_api_config_1_1) {
                word_lookup_api_config_1 = word_lookup_api_config_1_1;
            }
        ],
        execute: function () {
            exports_1("standardUkTiles", standardUkTiles = {
                name: 'Standard UK tile set',
                // The language and API.
                languageApiConfig: new word_lookup_api_config_1.WordLookupApiConfig('EN-world', 'http://api.wordnik.com/v4/word.json/{word}/definitions?limit=200&includeRelated=false&useCanonical=false&includeTags=false&sourceDictionaries=wiktionary&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5&callback=JSONP_CALLBACK', function response(result) {
                    console.log('Response callback being called - defined in the tileset');
                    return result.length > 0;
                }, function error(error) {
                    console.log('Error callback being called');
                    return false; // Just assume the word is not valid.
                }, 'Word validation API provided by api.wordnik.com'),
                // Format is points value => array of letter & number of tiles.
                tiles: {
                    '0': [
                        [' ', 2]
                    ],
                    '1': [
                        ['E', 12],
                        ['A', 9],
                        ['I', 9],
                        ['O', 8],
                        ['N', 6],
                        ['R', 6],
                        ['T', 6],
                        ['L', 4],
                        ['S', 4],
                        ['U', 4]
                    ],
                    '2': [
                        ['D', 4],
                        ['G', 3]
                    ],
                    '3': [
                        ['B', 2],
                        ['C', 2],
                        ['M', 2],
                        ['P', 2]
                    ],
                    '4': [
                        ['F', 2],
                        ['H', 2],
                        ['V', 2],
                        ['W', 2],
                        ['Y', 2]
                    ],
                    '5': [
                        ['K', 1]
                    ],
                    '8': [
                        ['J', 1],
                        ['X', 1],
                    ],
                    '10': [
                        ['Q', 1],
                        ['Z', 1]
                    ]
                }
            });
        }
    };
});
//# sourceMappingURL=standard-uk-tiles.js.map