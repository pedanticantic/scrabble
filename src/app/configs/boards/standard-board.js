System.register(["../../enums/premium-cell-type", "../tile-sets/standard-uk-tiles"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var premium_cell_type_1, standard_uk_tiles_1, standardBoard;
    return {
        setters: [
            function (premium_cell_type_1_1) {
                premium_cell_type_1 = premium_cell_type_1_1;
            },
            function (standard_uk_tiles_1_1) {
                standard_uk_tiles_1 = standard_uk_tiles_1_1;
            }
        ],
        execute: function () {
            exports_1("standardBoard", standardBoard = {
                name: 'Standard Board',
                size: 15,
                premiumCells: [],
                tileSets: [
                    standard_uk_tiles_1.standardUkTiles
                ]
            });
            standardBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.DLS,
                cells: [
                    [0, 3],
                    [2, 6],
                    [3, 0],
                    [3, 7],
                    [6, 2],
                    [6, 6],
                    [7, 3]
                ]
            });
            standardBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.TLS,
                cells: [
                    [1, 5],
                    [5, 1],
                    [5, 5]
                ]
            });
            standardBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.DWS,
                cells: [
                    [1, 1],
                    [2, 2],
                    [3, 3],
                    [4, 4],
                    [7, 7]
                ]
            });
            standardBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.TWS,
                cells: [
                    [0, 0],
                    [0, 7],
                    [7, 0],
                ]
            });
            // Now reflect them in the x-axis and then the y-axis to get a full board.
            for (var premiumIndex in standardBoard.premiumCells) {
                if (standardBoard.premiumCells.hasOwnProperty(premiumIndex)) {
                    var oneTypeData = standardBoard.premiumCells[premiumIndex];
                    var premiumCells = oneTypeData.cells;
                    for (var premiumCellIndex in premiumCells) {
                        if (premiumCells.hasOwnProperty(premiumCellIndex)) {
                            var thisCell = premiumCells[premiumCellIndex];
                            if (thisCell[0] < 7) {
                                premiumCells.push([14 - thisCell[0], thisCell[1]]);
                            }
                            if (thisCell[1] < 7) {
                                premiumCells.push([thisCell[0], 14 - thisCell[1]]);
                            }
                            if (thisCell[0] < 7 && thisCell[1] < 7) {
                                premiumCells.push([14 - thisCell[0], 14 - thisCell[1]]);
                            }
                        }
                    }
                }
            }
        }
    };
});
//# sourceMappingURL=standard-board.js.map