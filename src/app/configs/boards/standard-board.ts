import {PremiumCellType} from "../../enums/premium-cell-type";
import {standardUkTiles} from "../tile-sets/standard-uk-tiles";

export const standardBoard = {
    name: 'Standard Board',
    size: 15,
    premiumCells: [],
    tileSets: [
        standardUkTiles
    ]
};
standardBoard.premiumCells.push({
    cellType: PremiumCellType.DLS,
    cells: [
        [0, 3],
        [2, 6],
        [3, 0],
        [3, 7],
        [6, 2],
        [6, 6],
        [7, 3]
    ]
});
standardBoard.premiumCells.push({
    cellType: PremiumCellType.TLS,
    cells: [
        [1, 5],
        [5, 1],
        [5, 5]
    ]
});
standardBoard.premiumCells.push({
    cellType: PremiumCellType.DWS,
    cells: [
        [1, 1],
        [2, 2],
        [3, 3],
        [4, 4],
        [7, 7]
    ]
});
standardBoard.premiumCells.push({
    cellType: PremiumCellType.TWS,
    cells: [
        [0, 0],
        [0, 7],
        [7, 0],
    ]
});
// Now reflect them in the x-axis and then the y-axis to get a full board.
for(let premiumIndex in standardBoard.premiumCells) {
    if (standardBoard.premiumCells.hasOwnProperty(premiumIndex)) {
        let oneTypeData = standardBoard.premiumCells[premiumIndex];
        let premiumCells = oneTypeData.cells;
        for(let premiumCellIndex in premiumCells) {
            if (premiumCells.hasOwnProperty(premiumCellIndex)) {
                let thisCell = premiumCells[premiumCellIndex];
                if (thisCell[0] < 7) {
                    premiumCells.push([14 - thisCell[0], thisCell[1]]);
                }
                if (thisCell[1] < 7) {
                    premiumCells.push([thisCell[0], 14 - thisCell[1]]);
                }
                if (thisCell[0] < 7 && thisCell[1] < 7) {
                    premiumCells.push([14 - thisCell[0], 14 - thisCell[1]]);
                }
            }
        }
    }
}
