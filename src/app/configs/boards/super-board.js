System.register(["../../enums/premium-cell-type", "../tile-sets/super-uk-tiles"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var premium_cell_type_1, super_uk_tiles_1, superBoard;
    return {
        setters: [
            function (premium_cell_type_1_1) {
                premium_cell_type_1 = premium_cell_type_1_1;
            },
            function (super_uk_tiles_1_1) {
                super_uk_tiles_1 = super_uk_tiles_1_1;
            }
        ],
        execute: function () {
            exports_1("superBoard", superBoard = {
                name: 'Super Board',
                size: 21,
                premiumCells: [],
                tileSets: [
                    super_uk_tiles_1.superUkTiles
                ]
            });
            superBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.DLS,
                cells: [
                    [0, 3],
                    [0, 10],
                    [10, 0],
                    [3, 0],
                    [3, 6],
                    [5, 9],
                    [6, 3],
                    [6, 10],
                    [9, 5],
                    [9, 9],
                    [10, 6]
                ]
            });
            superBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.TLS,
                cells: [
                    [1, 4],
                    [4, 1],
                    [4, 8],
                    [8, 4],
                    [8, 8]
                ]
            });
            superBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.QLS,
                cells: [
                    [2, 5],
                    [5, 2]
                ]
            });
            superBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.DWS,
                cells: [
                    [1, 1],
                    [2, 2],
                    [4, 4],
                    [5, 5],
                    [6, 6],
                    [7, 7],
                    [10, 10],
                    [8, 1],
                    [9, 2],
                    [1, 8],
                    [2, 9]
                ]
            });
            superBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.TWS,
                cells: [
                    [3, 3],
                    [3, 10],
                    [7, 0],
                    [0, 7],
                    [10, 3],
                ]
            });
            superBoard.premiumCells.push({
                cellType: premium_cell_type_1.PremiumCellType.QWS,
                cells: [
                    [0, 0]
                ]
            });
            // Now reflect them in the x-axis and then the y-axis to get a full board.
            for (var premiumIndex in superBoard.premiumCells) {
                if (superBoard.premiumCells.hasOwnProperty(premiumIndex)) {
                    var oneTypeData = superBoard.premiumCells[premiumIndex];
                    var premiumCells = oneTypeData.cells;
                    for (var premiumCellIndex in premiumCells) {
                        if (premiumCells.hasOwnProperty(premiumCellIndex)) {
                            var thisCell = premiumCells[premiumCellIndex];
                            if (thisCell[0] < 10) {
                                premiumCells.push([20 - thisCell[0], thisCell[1]]);
                            }
                            if (thisCell[1] < 10) {
                                premiumCells.push([thisCell[0], 20 - thisCell[1]]);
                            }
                            if (thisCell[0] < 10 && thisCell[1] < 10) {
                                premiumCells.push([20 - thisCell[0], 20 - thisCell[1]]);
                            }
                        }
                    }
                }
            }
        }
    };
});
//# sourceMappingURL=super-board.js.map