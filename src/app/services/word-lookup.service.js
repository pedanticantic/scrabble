System.register(["@angular/core", "@angular/http", "rxjs/Observable", "../enums/validation-state"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, Observable_1, validation_state_1, WordLookupService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (validation_state_1_1) {
                validation_state_1 = validation_state_1_1;
            }
        ],
        execute: function () {
            WordLookupService = (function () {
                function WordLookupService(jsonp) {
                    this.jsonp = jsonp;
                    this.wordLookupApiConfig = null;
                    this.validatedWords = {};
                }
                WordLookupService.prototype.setLanguageApi = function (wordLookupApiConfig) {
                    this.wordLookupApiConfig = wordLookupApiConfig;
                };
                WordLookupService.prototype.getLanguageApi = function () {
                    return this.wordLookupApiConfig;
                };
                WordLookupService.prototype.validationResult = function (checkWord, newState) {
                    checkWord.setValidationResponse(newState);
                    this.validatedWords[checkWord.getWord()] = newState;
                };
                WordLookupService.prototype.seenBefore = function (checkWord) {
                    if (this.validatedWords.hasOwnProperty(checkWord.getWord())) {
                        checkWord.setValidationResponse(this.validatedWords[checkWord.getWord()]);
                        return true;
                    }
                    return false;
                };
                WordLookupService.prototype.validateWord = function (checkWord) {
                    var _this = this;
                    if (checkWord.getWordLength() <= 1) {
                        this.validationResult(checkWord, validation_state_1.ValidationState.NOT_VALID);
                        return;
                    }
                    // See if we've seen this word before.
                    if (this.seenBefore(checkWord)) {
                        return;
                    }
                    this.getWord(checkWord.asLowerCase())
                        .subscribe(function (result) {
                        _this.validationResult(checkWord, _this.wordLookupApiConfig.getResponseCallback()(result) ? validation_state_1.ValidationState.VALID : validation_state_1.ValidationState.NOT_VALID);
                    }, function (error) {
                        _this.validationResult(checkWord, _this.wordLookupApiConfig.getErrorCallback()(error) ? validation_state_1.ValidationState.VALID : validation_state_1.ValidationState.NOT_VALID);
                    });
                };
                WordLookupService.prototype.getWord = function (checkWord) {
                    var actualUrl = this.wordLookupApiConfig.getLookupUrl().replace(/\{word}/, checkWord);
                    return this.jsonp.get(actualUrl)
                        .map(this.extractData)
                        .catch(this.handleError);
                };
                WordLookupService.prototype.extractData = function (res) {
                    return res.json();
                };
                WordLookupService.prototype.handleError = function (error) {
                    console.log('handleError being called. Raw error: ', error);
                    // In a real world app, we might use a remote logging infrastructure
                    var errMsg;
                    if (error instanceof http_1.Response) {
                        var body = error.json() || '';
                        var err = body.error || JSON.stringify(body);
                        errMsg = error.status + " - " + (error.statusText || '') + " " + err;
                    }
                    else {
                        errMsg = error.message ? error.message : error.toString();
                    }
                    console.error(errMsg);
                    return Observable_1.Observable.throw(errMsg);
                };
                return WordLookupService;
            }());
            WordLookupService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [http_1.Jsonp])
            ], WordLookupService);
            exports_1("WordLookupService", WordLookupService);
        }
    };
});
//# sourceMappingURL=word-lookup.service.js.map