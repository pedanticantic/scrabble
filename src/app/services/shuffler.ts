/**
 * This is a utility that shuffles a given array.
 */
export class Shuffler {
    public shuffle(items: any[]) {
        var j, x, i;
        for (i = items.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = items[i - 1];
            items[i - 1] = items[j];
            items[j] = x;
        }
    }
}