import {Injectable}     from '@angular/core';
import {GAME_CONFIGS} from '../configs/game-config';
import {GameState} from '../models/game-state';
import {Player} from '../models/player';
import {TileBagGenerator} from '../generators/tile-bag-generator';
import {BoardGenerator} from "../generators/board-generator";
import {WordLookupService} from "./word-lookup.service";
import {GameStoreService} from "./game-store.service";

@Injectable()
export class GameService {
    private gameState: GameState = null;
    private gameConfig = null;
    private boardConfig = null;
    private tileConfig = null;
    private players: Player[] = null;

    constructor(private wordLookupService: WordLookupService, private gameStoreService: GameStoreService) {
        this.gameState = new GameState(wordLookupService, gameStoreService);
    }

    public static getConfigs() {
        return GAME_CONFIGS;
    }

    public getPlayers(): Player[] {
        return this.players;
    }

    public getGameConfig(): string {
        return this.gameConfig;
    }

    public resetGame(): void {
        this.players = null;
        this.gameConfig = null;
    }

    configureAndInitialiseGame(selectedConfig: string, players: Player[]) {
        // The selected config tells us the specifications of the board and the tile bag.
        // The game state needs to be initialised with instances of the board, tile bag and the players.
        this.gameConfig = selectedConfig;
        let configIndexes = this.gameConfig.split(':');
        this.boardConfig = GAME_CONFIGS.boards[configIndexes[0]];
        let tileSet = this.boardConfig.tileSets[configIndexes[1]];
        this.tileConfig = tileSet.tiles;
        this.wordLookupService.setLanguageDriver(tileSet.languageApiDriver);

        // We need to remove any unset players from the list.
        this.players = players.filter(function(player: Player) {
            return !!player.name;
        });

        this.initialiseGame();
    }

    public initialiseGame() {
        // So, we are starting the game.
        this.gameState.reset();

        // Build the board from the config.
        this.gameState.setBoard((new BoardGenerator()).generate(this.boardConfig));

        // Generate a tile bag, using the tiles spec.
        this.gameState.setTileBag((new TileBagGenerator()).generate(this.tileConfig));

        // Add the players to the game.
        this.gameState.setPlayers(this.players, true);

        // Store the initial state of the game.
        this.gameStoreService.saveInitialState(this.gameState);
        this.generateName();

        // Populate each player's rack, reset the score, etc.
        let self = this;
        this.players.forEach(function(player: Player) {
            player.initialise(self.gameState.getTileBag());
        });

        this.gameState.setInProgress(true);
        this.gameStoreService.saveCurrentGameState(this.gameState, 0);
    }

    public getGameState() {
        return this.gameState;
    }

    public setGameState(newGameState: GameState) {
        if (!this.gameState || !this.gameState.board) {
            this.gameState = newGameState;
        };
    }

    private generateName(): void {
        let gameIdx = 1;
        // Keep trying "New game (n)" until one "sticks".
        while (GameStoreService.updateName(this.gameState, 'New game ('+(gameIdx++)+')') != '') {}
    }
}
