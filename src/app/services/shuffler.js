System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Shuffler;
    return {
        setters: [],
        execute: function () {
            /**
             * This is a utility that shuffles a given array.
             */
            Shuffler = (function () {
                function Shuffler() {
                }
                Shuffler.prototype.shuffle = function (items) {
                    var j, x, i;
                    for (i = items.length; i; i--) {
                        j = Math.floor(Math.random() * i);
                        x = items[i - 1];
                        items[i - 1] = items[j];
                        items[j] = x;
                    }
                };
                return Shuffler;
            }());
            exports_1("Shuffler", Shuffler);
        }
    };
});
//# sourceMappingURL=shuffler.js.map