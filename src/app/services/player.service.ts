import {Injectable} from '@angular/core';

import {Player} from '../models/player';

@Injectable()
export class PlayerService {
    public newPlayers(): Player[] {
        let players = [null, null, null, null];
        //let players = ['One', 'Two', 'Three', 'Four'];
        let result: Player[] = [];
        for(var i = 0 ; i < 4 ; i++) {
            result.push(new Player(players[i], true, null));
        }

        return result;
    }

    // Remove the player at index playerIndex and add a new, empty one to the end of the list.
    public delete(players: Player[], playerIndex: number) {
        players.splice(playerIndex, 1);
        players.push(new Player(null, true, null));
    }
}