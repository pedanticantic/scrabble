System.register(["@angular/core", "./../configs/game-config", "./../models/game-state", "./../generators/tile-bag-generator", "./../generators/board-generator", "./word-lookup.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, game_config_1, game_state_1, tile_bag_generator_1, board_generator_1, word_lookup_service_1, GameService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (game_config_1_1) {
                game_config_1 = game_config_1_1;
            },
            function (game_state_1_1) {
                game_state_1 = game_state_1_1;
            },
            function (tile_bag_generator_1_1) {
                tile_bag_generator_1 = tile_bag_generator_1_1;
            },
            function (board_generator_1_1) {
                board_generator_1 = board_generator_1_1;
            },
            function (word_lookup_service_1_1) {
                word_lookup_service_1 = word_lookup_service_1_1;
            }
        ],
        execute: function () {
            GameService = (function () {
                function GameService(wordLookupService) {
                    this.wordLookupService = wordLookupService;
                    this.gameState = null;
                    this.gameConfig = null;
                    this.boardConfig = null;
                    this.tileConfig = null;
                    this.players = null;
                    console.log('Game service constructor');
                    this.gameState = new game_state_1.GameState(wordLookupService);
                }
                GameService.getConfigs = function () {
                    return game_config_1.GAME_CONFIGS;
                };
                GameService.prototype.getPlayers = function () {
                    return this.players;
                };
                GameService.prototype.getGameConfig = function () {
                    return this.gameConfig;
                };
                GameService.prototype.configureAndInitialiseGame = function (selectedConfig, players) {
                    console.log('Configure & initialise game');
                    // The selected config tells us the specifications of the board and the tile bag.
                    // The game state needs to be initialised with instances of the board, tile bag and the players.
                    this.gameConfig = selectedConfig;
                    var configIndexes = this.gameConfig.split(':');
                    this.boardConfig = game_config_1.GAME_CONFIGS.boards[configIndexes[0]];
                    var tileSet = this.boardConfig.tileSets[configIndexes[1]];
                    this.tileConfig = tileSet.tiles;
                    this.wordLookupService.setLanguageApi(tileSet.languageApiConfig);
                    // We need to remove any unset players from the list.
                    this.players = players.filter(function (player) {
                        return !!player.name;
                    });
                    this.initialiseGame();
                };
                GameService.prototype.initialiseGame = function () {
                    console.log('Initialise game');
                    // So, we are starting the game.
                    this.gameState.reset();
                    // Build the board from the config.
                    this.gameState.setBoard((new board_generator_1.BoardGenerator()).generate(this.boardConfig));
                    // Generate a tile bag, using the tiles spec.
                    this.gameState.setTileBag((new tile_bag_generator_1.TileBagGenerator()).generate(this.tileConfig));
                    console.log('Game state is now: ', this.gameState);
                    // Populate each player's rack, reset the score, etc.
                    var self = this;
                    this.players.forEach(function (player) {
                        player.initialise(self.gameState.getTileBag());
                    });
                    // Add the players to the game.
                    this.gameState.setPlayers(this.players, true);
                    this.gameState.setInProgress(true);
                };
                GameService.prototype.getGameState = function () {
                    return this.gameState;
                };
                return GameService;
            }());
            GameService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [word_lookup_service_1.WordLookupService])
            ], GameService);
            exports_1("GameService", GameService);
        }
    };
});
//# sourceMappingURL=game-service.js.map