import {WordLookupDriverInterface} from './WordLookupDriverInterface';
import {WordLookupResponse} from '../../value-objects/WordLookupResponse';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {ValidationState} from '../../enums/validation-state';

export class LinguaDriver implements WordLookupDriverInterface {
    getLanguageDescription(): string {
        return 'EN-world';
    }

    getCitation(): string | null {
        return 'Word validation API provided by lingua-robot.p.rapidapi.com';
    }

    lookupWord(httpClient: HttpClient, checkWord: string, callback: Function): void {
        let headers = new HttpHeaders({
            'x-rapidapi-key': import.meta.env.NG_APP_LINGUA_API_KEY,
            'x-rapidapi-host': 'lingua-robot.p.rapidapi.com'
        });
        httpClient.get(
            'https://lingua-robot.p.rapidapi.com/language/v1/entries/en/'+checkWord,
            {
              headers: headers,
            }
        )
        .subscribe({
            next: (response: {entries: any}) => {
                let definitions: string[] = [];
                response['entries'].forEach((entry: {lexemes: any}) => {
                  entry.lexemes.forEach((lexeme: {senses: any}) => {
                    lexeme.senses.forEach((sense: {definition: string}) => {
                      definitions.push(sense.definition);
                    })
                  })
                });
              callback(
                new WordLookupResponse(
                  (definitions.length === 0) ? ValidationState.NOT_VALID : ValidationState.VALID,
                  definitions
                )
              );
            },
            error: (error: HttpErrorResponse) => {
              callback(new WordLookupResponse(ValidationState.ERROR));
            }
        });
    }
}
