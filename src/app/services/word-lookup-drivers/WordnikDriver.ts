import {WordLookupResponse} from '../../value-objects/WordLookupResponse';
import {WordLookupDriverInterface} from './WordLookupDriverInterface';
import {ValidationState} from '../../enums/validation-state';
import {HttpClient} from '@angular/common/http';

export class WordnikDriver implements WordLookupDriverInterface {
    getLanguageDescription(): string {
        return 'EN-world';
    }

    getCitation(): string | null {
      return 'Word validation API provided by api.wordnik.com';
    }

    lookupWord(httpClient: HttpClient, checkWord: string, callback: Function): void {
console.log('Wordnik is checking this word: ', checkWord);
        let response: WordLookupResponse;
        if (checkWord.length === 3) {
            response = new WordLookupResponse(ValidationState.VALID, ['It is a 3 letter word']);
        } else {
            response = new WordLookupResponse(ValidationState.NOT_VALID);
        }

        callback(response);

        // This code used to do this, where the lookup URL was 'http://api.wordnik.com/v4/word.json/{word}/definitions?limit=5&includeRelated=false&useCanonical=false&includeTags=false&sourceDictionaries=wiktionary&api_key=<API Key>&callback=JSONP_CALLBACK',
        // API is in import.meta.env.NG_APP_WORDNIK_API_KEY
        // let actualUrl = this.wordLookupDriver.getLookupUrl().replace(/{word}/, checkWord);
        //     // In all honesty, I have absolutely no idea what the 2nd parameter here is supposed to be,
        //     // but it seems to work. I have searched for documentation, examples, etc, and... nothing.
        //     return httpClient.jsonp(actualUrl, 'something')
        //       .pipe(
        //         catchError(this.handleError)
        //       );
    }
}
