import {WordLookupResponse} from '../../value-objects/WordLookupResponse';
import {HttpClient} from '@angular/common/http';

export interface WordLookupDriverInterface {
    getLanguageDescription(): string;
    getCitation(): string | null;
    // @TODO: We need a better way of getting HttpClient into the driver.
    lookupWord(httpClient: HttpClient, checkWord: string, callback: Function): void;
}
