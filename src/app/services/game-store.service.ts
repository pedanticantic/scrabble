import {Injectable} from "@angular/core";
import {GameState} from "../models/game-state";
import {Board} from "../models/board";
import {BoardRow} from "../models/board-row";
import {BoardRowCell} from "../models/board-row-cell";
import {Player} from "../models/player";
import {TileBag} from "../models/tile-bag";
import {Tile} from "../models/tile";
import {TileRackTile} from "../models/tile-rack-tile";
import {WordLookupService} from "./word-lookup.service";
import {standardUkTiles} from "../configs/tile-sets/standard-uk-tiles";

@Injectable()
export class GameStoreService {
    static readonly GAMES_LIST_KEY = 'scrabble-games';

    public saveInitialState(gameState: GameState) {
        gameState.generateUniqueID();
        let gameID = gameState.getUniqueID();
        let storageStructure = {
            version: 1,
            name: gameState.getName(),
            board: this.boardToInitialStorage(gameState.getBoard()),
            players: this.playersToInitialStorage(gameState.getPlayers()),
            tileBag: this.tileBagToInitialStorage(gameState.getTileBag())
        };

        // Add the game id to the list of extant games.
        let currentGames = GameStoreService.getGamesIdList();
        currentGames.push(gameID);
        localStorage.setItem(GameStoreService.GAMES_LIST_KEY, JSON.stringify(currentGames));

        // Save this actual game.
        let initialGameKey = GameStoreService.buildInitialStateKey(gameID);
        localStorage.setItem(initialGameKey, JSON.stringify(storageStructure));

        // Do any housekeeping.
        GameStoreService.doHousekeeping();
    }

    public static getGamesIdList() {
        let currentGames = JSON.parse(localStorage.getItem(GameStoreService.GAMES_LIST_KEY));
        if (!(currentGames instanceof Array)) {
            currentGames = [];
        }

        return currentGames;
    }

    private static buildInitialStateKey(gameID: number): string {
        return GameStoreService.buildGameStateKey(gameID, 'initial');
    }

    private static buildGameStateKey(gameID: number, position: any): string {
        return GameStoreService.GAMES_LIST_KEY+':'+gameID+':'+position;
    }

    private boardToInitialStorage(board: Board) {
        // We need the size of the board and the coordinates of each premium square.
        let premiumCells = [];
        board.getBoardRows().forEach(function(boardRow: BoardRow, rowIndex) {
                boardRow.getRowCells().forEach(function(boardRowCell: BoardRowCell, cellIndex) {
                    let premiumCell = boardRowCell.getPremiumType();
                    if (premiumCell !== null) {
                        premiumCells.push({type: premiumCell, row: rowIndex, col: cellIndex});
                    }
                })
            }
        );

        return {size: board.getSize(), premiumCells: premiumCells}
    }

    private playersToInitialStorage(players: Player[]) {
        let playerList = [];
        players.forEach(function(player: Player) {
            playerList.push({isHuman: player.isHuman, name: player.getName()});
        });

        return playerList;
    }

    private tileBagToInitialStorage(tileBag: TileBag) {
        let tiles = [];
        tileBag.tiles.forEach(function(tile: Tile) {
            tiles.push({letter: tile.getLetter(), pointsValue: tile.getPointsValue()});
        });

        return tiles;
    }

    private static doHousekeeping() {
        // Remove any entry in the games list if it has no initial game state entry.
        let currentGames = GameStoreService.getGamesIdList();
        for (let gameIdx = currentGames.length - 1; gameIdx >= 0; gameIdx--) {
            let tmp = localStorage.getItem(GameStoreService.buildInitialStateKey(currentGames[gameIdx]));
            if (tmp == null) {
                currentGames.splice(gameIdx, 1);
                localStorage.setItem(GameStoreService.GAMES_LIST_KEY, JSON.stringify(currentGames));
            }
        }
    }

    public static updateName(gameState: GameState, newName: string): string {
        let gameUniqueID = gameState.getUniqueID();
        // Make sure the name is not empty.
        if (newName == '') {
            return 'Name must contain at least one letter';
        }
        if (newName.length > 15) {
            return 'Name must not be longer than 15 characters';
        }

        // Check if any other game has this name.
        let currentGames = GameStoreService.getGamesIdList();
        for (let gameIdx = 0 ; gameIdx < currentGames.length ; gameIdx++) {
            // We mustn't compare against our own game.
            if (currentGames[gameIdx] != gameUniqueID) {
                let initialGameKey = GameStoreService.buildInitialStateKey(currentGames[gameIdx]);
                let storedGame = JSON.parse(localStorage.getItem(initialGameKey));
                if (storedGame.name == newName) {
                    return 'Name already exists';
                }
            }
        }
        // Name is okay. Update it in the game state and in local storage.
        gameState.setName(newName);
        let initialGameKey = GameStoreService.buildInitialStateKey(gameUniqueID);
        let storedGame = JSON.parse(localStorage.getItem(initialGameKey));
        storedGame.name = newName;
        localStorage.setItem(initialGameKey, JSON.stringify(storedGame));

        return '';
    }

    public static loadGameInitialState(gameID: number) {
        let initialGameKey = GameStoreService.buildInitialStateKey(gameID);

        return JSON.parse(localStorage.getItem(initialGameKey));
    }

    public static removeGame(gameID: number): void {
        // Delete the initial state of the game.
        let initialGameKey = GameStoreService.buildInitialStateKey(gameID);
        localStorage.removeItem(initialGameKey);
        // Delete all the saved moves from this game.
        GameStoreService.removeMoves(gameID, 0);

        // Remove the game from the list of games.
        GameStoreService.doHousekeeping();
    }

    public saveCurrentGameState(gameState: GameState, position: number): void {
        let gameID = gameState.getUniqueID();
        let storageStructure = {
            version: 1,
            playerInfo: this.playersToCurrentState(gameState.getPlayers()),
            currentPlayerIndex: gameState.currentPlayerIndex,
            tileInfo: {
                onBoard: this.currentTilesOnBoard(gameState.getBoard()), // and which tiles were placed in the last move; letter for any blanks
                remaining: this.tilesRemaining(gameState.getTileBag())
            },
            gameIsFinished: gameState.isGameOver(),
        };

        // Save this actual game.
        let currentGameStateKey = GameStoreService.buildGameStateKey(gameID, position);
        localStorage.setItem(currentGameStateKey, JSON.stringify(storageStructure));

        // Remove any future game states in the case the players have undone a move, then played a different one.
        GameStoreService.removeMoves(gameID, 1+position);
    }

    private static removeMoves(gameID: number, fromPosition: number): void {
        let keyExists = false;
        do {
            let gameStateKey = GameStoreService.buildGameStateKey(gameID, fromPosition);
            keyExists = localStorage.getItem(gameStateKey) !== null;
            localStorage.removeItem(gameStateKey);
            ++fromPosition;
        } while (keyExists);
    }

    private playersToCurrentState(players: Player[]) {
        let playerState = [];
        players.forEach(function (player: Player) {
            let tileRack = [];
            player.tileRack.forEach(function (tileRackTile: TileRackTile) {
                // @TODO: See if it's worth adding a method to Tile to return a object representing the tile,
                // @TODO: for storage. Might be possible to use it in other places.
                let tile = tileRackTile.getTile();
                tileRack.push({
                    letter: tile.getLetter(),
                    pointsValue: tile.getPointsValue(),
                });
            });
            playerState.push({
                score: player.score,
                passes: player.numConsecutivePasses,
                scoreAdjustment: player.adjustmentAmount,
                tileRack: tileRack,
            });
        });
        return playerState;
    }

    private currentTilesOnBoard(board: Board) {
        let result = [];
        board.boardRows.forEach(function (boardRow) {
            let thisRow = [];
            boardRow.boardRowCells.forEach(function (cell: BoardRowCell) {
                let tile = null;
                let letterForBlank = null;
                let tileState = null;
                let pointsValue = null;
                if (cell.hasTile()) {
                    let tileOnBoard = cell.getTile();
                    tile = tileOnBoard.getLetter();
                    letterForBlank = tileOnBoard.getLetterForBlank();
                    tileState = cell.getTileState(); // Was it just played, or was it already there, etc?
                    pointsValue = tileOnBoard.getPointsValue();
                }
                thisRow.push({
                    tile: tile,
                    letterForBlank: letterForBlank,
                    tileState: tileState,
                    pointsValue: pointsValue
                });
            });
            result.push(thisRow);
        });
        return result;
    }

    private tilesRemaining(tileBag: TileBag) {
        let result = [];
        tileBag.tiles.forEach(function (tile: Tile) {
            result.push({letter: tile.getLetter(), pointsValue: tile.getPointsValue()});
        });

        return result;
    }

    public static undoMove(gameState: GameState) {
        // Subtract 1 from the move number and load that state from storage.
        let previousPosition = gameState.decrementMoveNumber();
        GameStoreService.restorePosition(gameState, previousPosition);
    }

    public static hasFutureMoves(gameState: GameState): boolean {
        let gameStateKey = this.buildGameStateKey(gameState.getUniqueID(), 1+gameState.getNumMoves());

        return localStorage.getItem(gameStateKey) != null;
    }

    public static redoMove(gameState: GameState) {
        // Add 1 to the move number and load that state from storage.
        let nextPosition = gameState.incrementMoveNumber();
        GameStoreService.restorePosition(gameState, nextPosition);
    }

    private static restorePosition(gameState: GameState, positionNumber: number) {
        let gameStateKey = this.buildGameStateKey(gameState.getUniqueID(), positionNumber);
        let previousState = JSON.parse(localStorage.getItem(gameStateKey));

        // Update the state of the game (or its components) to match the saved state.
        gameState.restoreState(previousState.currentPlayerIndex, previousState.gameIsFinished);
        gameState.getPlayers().forEach(function (player: Player, index: number) {
            player.restoreState(previousState.playerInfo[index]);
        });
        gameState.getBoard().restoreState(previousState.tileInfo.onBoard);
        gameState.getTileBag().restoreState(previousState.tileInfo.remaining);
    }

    private static restoreLatestPosition(gameState: GameState): void {
        // We need to loop through the keys for the saved positions for this game, until we hit the last
        // one, and restore that one. We must make sure the internal move count is correct at the end.
        let seekPosition = 1;
        let previousRawState = null;
            do {
            let gameStateKey = this.buildGameStateKey(gameState.getUniqueID(), seekPosition);
            previousRawState = localStorage.getItem(gameStateKey);
            if (previousRawState != null) {
                gameState.incrementMoveNumber();
                seekPosition++;
            }
        } while (previousRawState != null);
        GameStoreService.restorePosition(gameState, seekPosition-1);
    }

    public restoreGame(
        gameID: number,
        wordLookupService: WordLookupService,
        gameStoreService: GameStoreService
    ): GameState {
        // Load the config from storage.
        let initialGameKey = GameStoreService.buildInitialStateKey(gameID);
        let storageStructure = JSON.parse(localStorage.getItem(initialGameKey));

        // Apply the saved config, etc to the game state.
        // Initialise a game state.
        // standardUkTiles
        // @TODO: FIXME: we need to somehow store in the saved game which API config to use.
        wordLookupService.setLanguageDriver(standardUkTiles.languageApiDriver);
        let gameState = new GameState(wordLookupService, gameStoreService);
        gameState.setUniqueID(gameID);
        gameState.restoreBoard(storageStructure.board);
        gameState.restorePlayers(storageStructure.players);
        gameState.restoreTileBag(storageStructure.tileBag);
        gameState.setName(storageStructure.name);
        gameState.setInProgress(true);
        // Restore latest version of the game.
        GameStoreService.restoreLatestPosition(gameState);

        return gameState;
    }
}
