import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {PlayedWord} from "../models/played-word";
import {ValidationState} from "../enums/validation-state";
import {WordLookupResponse} from "../value-objects/WordLookupResponse";
import {throwError} from 'rxjs';
import {WordLookupDriverInterface} from './word-lookup-drivers/WordLookupDriverInterface';

@Injectable()
export class WordLookupService {
    private wordLookupDriver: WordLookupDriverInterface = null;
    private validatedWords = {};

    constructor (private httpClient: HttpClient) {}

    public setLanguageDriver(wordLookupDriver: WordLookupDriverInterface) {
        this.wordLookupDriver = wordLookupDriver;
    }

    public getLanguageDriver(): WordLookupDriverInterface {
        return this.wordLookupDriver;
    }

    private validationResult(checkWord: PlayedWord, validationResult: WordLookupResponse) {
        checkWord.setValidationResponse(validationResult);
        // Unless there was an error, add this word to the list of validated words.
        if (validationResult.getValidState() != ValidationState.ERROR) {
            this.validatedWords[checkWord.getWord()] = validationResult;
        }
    }

    private seenBefore(checkWord: PlayedWord): boolean {
        if (this.validatedWords.hasOwnProperty(checkWord.getWord())) {
            checkWord.setValidationResponse(this.validatedWords[checkWord.getWord()]);

            return true;
        }

        return false;
    }

    public validateWord(checkWord: PlayedWord) {
        if (checkWord.getWordLength() <= 1) {
            this.validationResult(checkWord, new WordLookupResponse(ValidationState.NOT_VALID));

            return;
        }

        // See if we've seen this word before.
        if (this.seenBefore(checkWord)) {
            return;
        }

        this.wordLookupDriver.lookupWord(this.httpClient, checkWord.asLowerCase(), (wordLookupResponse: WordLookupResponse): void => {
          if (
            wordLookupResponse.getValidState() === ValidationState.VALID ||
            wordLookupResponse.getValidState() === ValidationState.NOT_VALID ||
            wordLookupResponse.getValidState() === ValidationState.ERROR
          ) {
            this.validationResult(checkWord, wordLookupResponse);
          }
        });
    }

    private handleError (errorResponse: HttpErrorResponse | any) {
        console.log('handleError being called. Raw error: ', errorResponse);
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (errorResponse instanceof HttpErrorResponse) {
            const err = errorResponse.error || JSON.stringify(errorResponse);
            errMsg = `${errorResponse.status} - ${errorResponse.statusText || ''} ${err}`;
        } else {
            errMsg = errorResponse.message ? errorResponse.message : errorResponse.toString();
        }
        console.error(errMsg);
        return throwError(() => new Error(errMsg));
    }
}
