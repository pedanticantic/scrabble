System.register(["./../models/tile", "./../models/tile-bag"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var tile_1, tile_bag_1, TileBagGenerator;
    return {
        setters: [
            function (tile_1_1) {
                tile_1 = tile_1_1;
            },
            function (tile_bag_1_1) {
                tile_bag_1 = tile_bag_1_1;
            }
        ],
        execute: function () {
            TileBagGenerator = (function () {
                function TileBagGenerator() {
                }
                TileBagGenerator.prototype.generate = function (tileConfig) {
                    var tileBag = new tile_bag_1.TileBag();
                    for (var pointsIndex in tileConfig) {
                        if (tileConfig.hasOwnProperty(pointsIndex)) {
                            var tilesWithPoints = tileConfig[pointsIndex];
                            for (var tileIndex in tilesWithPoints) {
                                if (tilesWithPoints.hasOwnProperty(tileIndex)) {
                                    var thisTileDef = tilesWithPoints[tileIndex];
                                    for (var i = 0; i < thisTileDef[1]; i++) {
                                        tileBag.add(new tile_1.Tile(thisTileDef[0], parseInt(pointsIndex, 10)));
                                    }
                                }
                            }
                        }
                    }
                    tileBag.shuffleBag();
                    return tileBag;
                };
                return TileBagGenerator;
            }());
            exports_1("TileBagGenerator", TileBagGenerator);
        }
    };
});
//# sourceMappingURL=tile-bag-generator.js.map