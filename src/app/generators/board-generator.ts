import {Board} from '../models/board';
import {BoardRow} from "../models/board-row";
import {BoardRowCell} from "../models/board-row-cell";
import {CellCoordinates} from "../models/cell-coordinates";

export class BoardGenerator {
    generate(boardConfig): Board {
        // Start by creating an nxn board with no premium squares.
        let size = boardConfig.size;
        let board = new Board(size);
        for(let row = 0 ; row < size ; row++) {
            let thisRow = new BoardRow();
            for(let col = 0 ; col < size ; col++) {
                thisRow.addCell(new BoardRowCell(new CellCoordinates(row, col)));
            }
            board.addRow(thisRow);
        }
        // Now go through the premium squares and set the appropriate cell in the board.
        for(let premiumCellsIndex in boardConfig.premiumCells) {
            if (boardConfig.premiumCells.hasOwnProperty(premiumCellsIndex)) {
                let premiumCellData = boardConfig.premiumCells[premiumCellsIndex];
                let premiumType = premiumCellData.cellType;
                let premiumCells = premiumCellData.cells;
                for(let premiumCellIndex in premiumCells) {
                    if (premiumCells.hasOwnProperty(premiumCellIndex)) {
                        let premiumCell = premiumCells[premiumCellIndex];
                        board.setPremiumCell(premiumType, premiumCell[0], premiumCell[1]);
                    }
                }
            }
        }

        return board;
    }
}
