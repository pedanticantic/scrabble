import {Tile} from '../models/tile';
import {TileBag} from '../models/tile-bag';

export class TileBagGenerator {
    generate(tileConfig): TileBag {
        var tileBag: TileBag = new TileBag();

        for(var pointsIndex in tileConfig) {
            if (tileConfig.hasOwnProperty(pointsIndex)) {
                let tilesWithPoints = tileConfig[pointsIndex];
                for(let tileIndex in tilesWithPoints) {
                    if (tilesWithPoints.hasOwnProperty(tileIndex)) {
                        var thisTileDef = tilesWithPoints[tileIndex];
                        for (var i = 0; i < thisTileDef[1]; i++) {
                            tileBag.add(new Tile(
                                thisTileDef[0] as string,
                                parseInt(pointsIndex, 10)
                            ));
                        }
                    }
                }
            }
        }

        tileBag.shuffleBag();

        return tileBag;
    }
}
