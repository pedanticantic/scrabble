System.register(["./../models/board", "./../models/board-row", "./../models/board-row-cell", "../models/cell-coordinates"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var board_1, board_row_1, board_row_cell_1, cell_coordinates_1, BoardGenerator;
    return {
        setters: [
            function (board_1_1) {
                board_1 = board_1_1;
            },
            function (board_row_1_1) {
                board_row_1 = board_row_1_1;
            },
            function (board_row_cell_1_1) {
                board_row_cell_1 = board_row_cell_1_1;
            },
            function (cell_coordinates_1_1) {
                cell_coordinates_1 = cell_coordinates_1_1;
            }
        ],
        execute: function () {
            BoardGenerator = (function () {
                function BoardGenerator() {
                }
                BoardGenerator.prototype.generate = function (boardConfig) {
                    // Start by creating an nxn board with no premium squares.
                    var size = boardConfig.size;
                    var board = new board_1.Board(size);
                    for (var row = 0; row < size; row++) {
                        var thisRow = new board_row_1.BoardRow();
                        for (var col = 0; col < size; col++) {
                            thisRow.addCell(new board_row_cell_1.BoardRowCell(new cell_coordinates_1.CellCoordinates(row, col)));
                        }
                        board.addRow(thisRow);
                    }
                    // Now go through the premium squares and set the appropriate cell in the board.
                    for (var premiumCellsIndex in boardConfig.premiumCells) {
                        if (boardConfig.premiumCells.hasOwnProperty(premiumCellsIndex)) {
                            var premiumCellData = boardConfig.premiumCells[premiumCellsIndex];
                            var premiumType = premiumCellData.cellType;
                            var premiumCells = premiumCellData.cells;
                            for (var premiumCellIndex in premiumCells) {
                                if (premiumCells.hasOwnProperty(premiumCellIndex)) {
                                    var premiumCell = premiumCells[premiumCellIndex];
                                    board.setPremiumCell(premiumType, premiumCell[0], premiumCell[1]);
                                }
                            }
                        }
                    }
                    return board;
                };
                return BoardGenerator;
            }());
            exports_1("BoardGenerator", BoardGenerator);
        }
    };
});
//# sourceMappingURL=board-generator.js.map