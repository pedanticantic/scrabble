import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule}   from '@angular/forms';
import {RouterModule}  from '@angular/router';
// import {ModalModule}   from 'ngx-bootstrap';
import {HttpClientModule, HttpClientJsonpModule} from '@angular/common/http';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {AppComponent}  from './components/app.component';
import {PlayerService}         from './services/player.service';
import {StartGameComponent}    from './components/start-game.component';
import {WelcomeComponent} from "./components/welcome-component";
import {PlayGameComponent} from "./components/play-game.component";
import {TileComponent} from "./components/tile.componant";
import {WordLookupService} from "./services/word-lookup.service";
import {GameService} from "./services/game-service";
import {GameStoreService} from "./services/game-store.service";
import {ListGamesComponent} from "./components/list-games-component";
import {BsModalService} from 'ngx-bootstrap/modal';

@NgModule({
    declarations: [],
  imports: [
    BrowserModule,
    FormsModule,
    // ModalModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        component: WelcomeComponent
      },
      {
        path: 'startGame',
        component: StartGameComponent
      },
      {
        path: 'savedGames',
        component: ListGamesComponent
      },
      {
        path: 'game',
        component: PlayGameComponent
      }
    ]),
    FontAwesomeModule,
    HttpClientModule,
    HttpClientJsonpModule,
    AppComponent,
    StartGameComponent,
    WelcomeComponent,
    PlayGameComponent,
    TileComponent,
    ListGamesComponent
  ],
    providers: [
        PlayerService,
        GameService,
        WordLookupService,
        GameStoreService,
        BsModalService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
