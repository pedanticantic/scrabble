import {Component, OnInit} from '@angular/core';
import {Router, RouterLink} from '@angular/router';

import {GameService} from '../services/game-service';
import {Player} from '../models/player';
import {PlayerService} from '../services/player.service';
import {NgForOf, NgIf} from '@angular/common';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'my-startGame',
  templateUrl: '../templates/start-game.html',
  imports: [
    NgForOf,
    FormsModule,
    RouterLink,
    NgIf
  ],
  providers: [
    PlayerService
  ]
})
export class StartGameComponent implements OnInit {
    gameConfigs;
    selectedGameConfig: string = '0:0';
    players: Player[];

    constructor(private playerService: PlayerService, private gameService: GameService, private router: Router) {}

    ngOnInit(): void {
        this.gameConfigs = GameService.getConfigs();
        // If player and config information exists (eg if we are restarting a game), then populate
        // the appropriate properties.
        if (this.gameService.getPlayers() != null && this.gameService.getGameConfig() != null) {
            this.players = this.gameService.getPlayers();
            this.selectedGameConfig = this.gameService.getGameConfig();
        } else {
            this.players = this.playerService.newPlayers();
        }
    }

    onDelete(playerIndex: number): void {
        this.playerService.delete(this.players, playerIndex);
    }

    noPlayersSelected(): boolean {
        for(let index = 0 ; index < this.players.length ; index++) {
            if (this.players[index].name) {
                return false;
            }
        }

        return true;
    }

    hasDuplicatePlayers(): boolean {
        // Check there are no duplicates.
        for(let outerIndex = 0 ; outerIndex < (this.players.length - 1); outerIndex++) {
            if (this.players[outerIndex].name != null && this.players[outerIndex].name != '') {
                for (let innerIndex = outerIndex + 1; innerIndex < this.players.length; innerIndex++) {
                    if (this.players[outerIndex].name == this.players[innerIndex].name) {

                        return true;
                    }
                }
            }
        }

        return false;
    }


    startGame(): void {
        this.gameService.configureAndInitialiseGame(this.selectedGameConfig, this.players);
        this.router.navigateByUrl('/game');
    }
}
