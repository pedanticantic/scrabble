System.register(["@angular/core", "../models/tile", "../models/tile-rack-tile", "../models/board-row-cell"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, tile_1, tile_rack_tile_1, board_row_cell_1, TileComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (tile_1_1) {
                tile_1 = tile_1_1;
            },
            function (tile_rack_tile_1_1) {
                tile_rack_tile_1 = tile_rack_tile_1_1;
            },
            function (board_row_cell_1_1) {
                board_row_cell_1 = board_row_cell_1_1;
            }
        ],
        execute: function () {
            TileComponent = (function () {
                function TileComponent() {
                    this.tileRackTile = null;
                    this.boardRowCell = null;
                    this.showLetterIfBlank = '0';
                }
                TileComponent.prototype.getExtraClasses = function () {
                    var result = [];
                    if (this.tileRackTile != null) {
                        result.push(this.tileRackTile.getCssSubclass());
                        if (this.tileRackTile.getTile().isBlank()) {
                            result.push('blank-tile');
                        }
                    }
                    if (this.boardRowCell != null) {
                        result.push(this.boardRowCell.getCssSubclass());
                        if (this.boardRowCell.getTile().isBlank()) {
                            result.push('blank-tile');
                        }
                    }
                    return result.join(' ');
                };
                return TileComponent;
            }());
            __decorate([
                core_1.Input(),
                __metadata("design:type", tile_1.Tile)
            ], TileComponent.prototype, "tile", void 0);
            __decorate([
                core_1.Input(),
                __metadata("design:type", tile_rack_tile_1.TileRackTile)
            ], TileComponent.prototype, "tileRackTile", void 0);
            __decorate([
                core_1.Input(),
                __metadata("design:type", board_row_cell_1.BoardRowCell)
            ], TileComponent.prototype, "boardRowCell", void 0);
            __decorate([
                core_1.Input(),
                __metadata("design:type", String)
            ], TileComponent.prototype, "showLetterIfBlank", void 0);
            TileComponent = __decorate([
                core_1.Component({
                    selector: 'tile-container',
                    templateUrl: '../templates/one-tile.html'
                }),
                __metadata("design:paramtypes", [])
            ], TileComponent);
            exports_1("TileComponent", TileComponent);
        }
    };
});
//# sourceMappingURL=tile.componant.js.map