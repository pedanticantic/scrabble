import {Component, ViewChild, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {ModalDirective} from 'ngx-bootstrap/modal';

import {GameService} from "../services/game-service";
import {GameState} from "../models/game-state";
import {BoardRowCell} from "../models/board-row-cell";
import {WordDirection} from "../enums/word-direction";
import {GameStoreService} from "../services/game-store.service";
import {FormsModule} from '@angular/forms';
import {NgClass, NgForOf, NgIf, NgSwitch, NgSwitchCase} from '@angular/common';
import {TileComponent} from './tile.componant';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faCheckCircle, faEdit, faExclamationCircle, faList, faStar} from '@fortawesome/free-solid-svg-icons';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import {faExclamation} from '@fortawesome/free-solid-svg-icons/faExclamation';
import {faQuestionCircle} from '@fortawesome/free-solid-svg-icons/faQuestionCircle';
// import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'play-game-selector',
  imports: [
    FormsModule,
    ModalDirective,
    NgClass,
    TileComponent,
    NgSwitch,
    NgForOf,
    NgIf,
    NgSwitchCase,
    FaIconComponent
  ],
  templateUrl: '../templates/play-game.html'
})
export class PlayGameComponent {
    @ViewChild('directionModal') public directionModal: ModalDirective;
    @ViewChild('chooseLetterModal') public chooseLetterModal: ModalDirective;
    @ViewChild('endOfGameModal') public endOfGameModal: ModalDirective;
    @ViewChild('editNameModal') public editNameModal: ModalDirective;

    @HostListener('document:keypress', ['$event'])
    public keypress(e: KeyboardEvent) {
        // Do nothing if certain modals are open.
        if (this.editNameModal.isShown) {
            return;
        }

        // Pressing a key means different things at different times (and often nothing at all).
        // Different entities know how to handle it, so this one tries to, and if it can't, passes it
        // on to the game state.
        let keyToUpper = e.key.toUpperCase();
        if (e.keyCode == 27) {
            // They pressed ESC.
            if (this.isPlayerPlacingTiles()) {
                this.cancelPlayTiles();
            } else if (this.isPlayerExchangingTiles()) {
                this.cancelExchange();
            } else if (this.directionModal.isShown) {
                this.hideDirectionModal();
            }
        } else if (e.keyCode == 13) {
            // They pressed RETURN.
            if (this.isPlayerPlacingTiles() && this.canConfirmPlayTiles()) {
                this.confirmPlayTiles();
            } else if (this.isPlayerExchangingTiles() && this.canConfirmExchange()) {
                this.confirmExchange();
            }
        } else if (e.keyCode == 8 || e.keyCode == 46) {
            // The pressed BACKSPACE or DEL.
            if (this.isPlayerPlacingTiles() && this.canRemoveLastTile()) {
                this.removeLastTile();
            }
        } else if (this.chooseLetterModal.isShown) {
            // The "Blank letter" chooser is open, so that will handle the keypress.
            this.selectLetter(keyToUpper);
        } else if (this.directionModal.isShown) {
            // The direction chooser is open. See if they presses 'a' or 'd'.
            if (keyToUpper == 'A') {
                this.playAcross();
            } else if (keyToUpper == 'D') {
                this.playDown();
            }
        } else if (this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles()) {
            let tileRackTiles = this.gameState.currentPlayer().getTileRack();
            for (let index = 0; index < tileRackTiles.length; index++) {
                if (tileRackTiles[index].getTile().getLetter() == keyToUpper) {
                    if (!tileRackTiles[index].getHasBeenSelected()) {
                        // This handles whether they are placing tiles or exchanging them.
                        this.selectTile(index);
                        break;
                    }
                }
            }
        } else if (keyToUpper == 'S' && this.canPlayerShuffle()) {
            this.playerShuffle();
        } else if (keyToUpper == 'P' && this.canPlayerPass()) {
            this.playerPass();
        } else if (keyToUpper == 'X' && this.canPlayerExchange()) {
            this.playerExchange();
        }
    }

    public gameState: GameState;
    private startClickedCell: BoardRowCell;
    private modalConfig = {
        ignoreBackdropClick: true,
        keyboard: false,
        focus: false
    };
    lookedUpWord: string = null;
    errorMsg: string = null;
    public gameNameCopy: string = null;
    public editNameErrorMessage: string = null;

    // Modal properties
    @ViewChild('modal')
    modal: any;

    constructor(
        private gameService: GameService,
        private router: Router
    ) {
        this.gameState = gameService.getGameState();
        if (!this.gameState.isInProgress()) {
            this.router.navigateByUrl('/');
        }
    }

    public playerShuffle() {
        this.gameState.currentPlayer().shuffleRack();
    }

    public playerPass() {
        let currentPlayerName = this.gameState.currentPlayer().getName();
        if (this.gameState.pass()) {
            alert("Player " + currentPlayerName + " has passed.\n\n(Game ends if all players pass twice in a row)");
            if (this.gameState.isGameOver()) {
                this.endOfGameModal.config = this.modalConfig;
                this.endOfGameModal.show();
            }
        }
    }

    public playerExchange() {
        this.gameState.exchange();
    }

    public cellClick(boardRowCell: BoardRowCell) {
        // If they're playing tiles or exchanging tiles or the game is over, just do nothing.
        if (this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() || this.gameIsOver()) {

            return;
        }

        this.startClickedCell = boardRowCell;
        if (this.gameState.startWordPositionIsValid(boardRowCell)) {
            // See if they are restricted to one direction or the other. If so, don't ask them!
            let wordDirections = this.gameState.getAllowedDirectionsFromCell(boardRowCell);
            switch (wordDirections.length) {
                case 0:
                    alert('You cannot start a word from here!');
                    break;
                case 1:
                    // Only 1 direction allowed, so choose it!
                    this.playDirection(wordDirections[0]);
                    break;
                default:
                    // Multiple directions (ie across and down) allowed, so ask user which one they want.
                    this.directionModal.config = this.modalConfig;
                    this.directionModal.show();
                    break;
            }
        } else {
            alert('Sorry, you can\'t start a word there in this turn');
        }
    }

    private playDirection(wordDirection: WordDirection) {
        this.gameState.startPlayingTiles(this.startClickedCell, wordDirection);
    }

    public hideDirectionModal(): void {
        this.directionModal.hide();
    }

    public cancelChooseLetterModal(): void {
        // Remove the last tile played, which will be the blank tile with no letter.
        this.removeLastTile();
        this.chooseLetterModal.hide();
    }

    public acceptChooseLetterModal(): void {
        this.chooseLetterModal.hide();
    }

    public playAcross(): void {
        this.hideDirectionModal();
        this.playDirection(WordDirection.ACROSS);
    }

    public playDown(): void {
        this.hideDirectionModal();
        this.playDirection(WordDirection.DOWN);
    }

    public selectTile(tileRackTileIndex: number): void {
        if (this.gameState.canSelectATile()) {
            if (!this.gameState.currentPlayer().hasTileBeenSelected(tileRackTileIndex)) {
                if (this.gameState.playerTurn.isInProgress()) {
                    if (this.gameState.playTile(tileRackTileIndex)) {
                        // If the user just played a blank, ask them which letter they want it to be.
                        let tileRackTile = this.gameState.currentPlayer().getTileRack()[tileRackTileIndex];
                        if (tileRackTile.getTile().isBlank()) {
                            this.gameState.setLastPlayedBlank(tileRackTile.getTile());
                            this.chooseLetterModal.config = this.modalConfig;
                            this.chooseLetterModal.show();
                        }
                    }
                } else {
                    if (this.gameState.exchangeTiles.isInProgress()) {
                        this.gameState.exchangeOneTile(tileRackTileIndex);
                    }
                }
            }
        }
    }

    public isPlayerPlacingTiles(): boolean {
        return this.gameState.playerTurn.isInProgress();
    }

    public removeLastTile(): void {
        if (this.tilesHaveBeenPlayed()) {
            this.gameState.removeLastTile();
        }
    }

    public centreSquareHasTile(): boolean {
        return this.gameState.getBoard().hasTileInCentre();
    }

    public tilesHaveBeenPlayed(): boolean {
        return this.gameState.tilesHaveBeenPlayed();
    }

    public cancelPlayTiles(): void {
        this.gameState.cancelPlayTiles();
    }

    public confirmPlayTiles(): void {
        if (this.tilesHaveBeenPlayed() && this.centreSquareHasTile() && this.isTouchingExistingTile() && !this.hasSingleLetterWord()) {
            this.gameState.confirmPlayTiles();
            if (this.gameState.isGameOver()) {
                this.endOfGameModal.config = this.modalConfig;
                this.endOfGameModal.show();
            }
        }
    }

    public isTouchingExistingTile(): boolean {
        return this.gameState.isTouchingExistingTile();
    }

    public hasSingleLetterWord(): boolean {
        return this.gameState.hasSingleLetterWord();
    }

    public isFirstMove(): boolean {
        return this.gameState.isFirstMove();
    }

    public afterFirstMove(): boolean {
        return this.gameState.afterFirstMove();
    }

    public selectLetter(whichLetter: string): void {
        if (this.gameState.selectLetter(whichLetter)) {
            this.acceptChooseLetterModal();
        }
    }

    public isPlayerExchangingTiles(): boolean {
        return this.gameState.exchangeTiles.isInProgress();
    }

    public unexchangeTile(exchangeTilesIndex): void {
        this.gameState.unexchangeTiles(exchangeTilesIndex);
    }

    public cancelExchange(): void {
        this.gameState.unexchangeAllTiles();
    }

    public confirmExchange(): void {
        this.gameState.confirmExchange();
    }

    public enoughTilesInBag(): boolean {
        return this.gameState.enoughTilesInBag();
    }

    public closeEndOfGameModal(): void {
        this.endOfGameModal.hide();
    }

    public gameIsOver(): boolean {
        return this.gameState.isEndOfGame();
    }

    public newGame(): void {
        this.gameService.resetGame();
        this.router.navigateByUrl('/startGame');
    }

    public replayGame(): void {
        // Start the game again, using the same config as before.
        this.gameService.initialiseGame();
    }

    public redisplayAdjustments(): void {
        this.endOfGameModal.show();
    }

    public checkAbandonGame() {
        return window.confirm('Are you sure?');
    }

    public canPlayerShuffle(): boolean {
        return !this.gameIsOver();
    }
    public canPlayerPass(): boolean {
        return !(this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() || this.gameIsOver());
    }
    public canPlayerExchange(): boolean {
        return !(this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() || this.gameIsOver());
    }
    public canRemoveLastTile(): boolean {
        return this.tilesHaveBeenPlayed();
    }
    public canConfirmPlayTiles(): boolean {
        return this.tilesHaveBeenPlayed() && this.centreSquareHasTile() && this.isTouchingExistingTile() && !this.hasSingleLetterWord();
    }
    public canConfirmExchange(): boolean {
        return this.gameState.exchangeTiles.hasTiles();
    }

    public editName(): void {
        this.gameNameCopy = this.gameState.getName();
        this.editNameErrorMessage = null;
        this.editNameModal.show();
    }
    public closeEditNameGameModal(): void {
        this.editNameModal.hide();
    }
    public saveEditNameGame(): void {
        let result = GameStoreService.updateName(this.gameState, this.gameNameCopy);
        if (result == '') {
            this.editNameModal.hide();
        }
        this.editNameErrorMessage = result;
    }

    public undoMoveAllowed(): boolean {
        return this.afterFirstMove() && !this.performingAction();
    }

    public redoMoveAllowed(): boolean {
        return this.hasFutureMoves() && !this.performingAction();
    }

    private performingAction(): boolean {
        return this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() /*||
            this.editNameModal.isShown || this.directionModal.isShown || this.chooseLetterModal.isShown*/;

    }

    public undoMove(): void {
        if (!confirm('Are you sure?')) {
            return;
        }
        // Okay, load the previous move from storage and update all the necessary data.
        GameStoreService.undoMove(this.gameState);
    }

    public hasFutureMoves(): boolean {
        return GameStoreService.hasFutureMoves(this.gameState);
    }

    public redoMove(): void {
        if (!confirm('Are you sure?')) {
            return;
        }
        // Okay, load the next move from storage and update all the necessary data.
        GameStoreService.redoMove(this.gameState);
    }

  protected readonly faEdit = faEdit;
  protected readonly faStar = faStar;
  protected readonly faExclamationCircle = faExclamationCircle;
  protected readonly faCheckCircle = faCheckCircle;
  protected readonly faList = faList;
  protected readonly faTimesCircle = faTimesCircle;
  protected readonly faExclamation = faExclamation;
  protected readonly faQuestionCircle = faQuestionCircle;
}
