System.register(["@angular/core", "@angular/router", "./../services/game-service", "./../services/player.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, game_service_1, player_service_1, StartGameComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (game_service_1_1) {
                game_service_1 = game_service_1_1;
            },
            function (player_service_1_1) {
                player_service_1 = player_service_1_1;
            }
        ],
        execute: function () {
            StartGameComponent = (function () {
                function StartGameComponent(playerService, gameService, router) {
                    this.playerService = playerService;
                    this.gameService = gameService;
                    this.router = router;
                    this.selectedGameConfig = '0:0';
                }
                StartGameComponent.prototype.ngOnInit = function () {
                    this.gameConfigs = GameService.getConfigs();
                    // If player and config information exists (eg if we are restarting a game), then populate
                    // the appropriate properties.
                    if (this.gameService.getPlayers() != null && this.gameService.getGameConfig() != null) {
                        this.players = this.gameService.getPlayers();
                        this.selectedGameConfig = this.gameService.getGameConfig();
                    }
                    else {
                        this.players = this.playerService.newPlayers();
                    }
                };
                StartGameComponent.prototype.onDelete = function (playerIndex) {
                    this.playerService.delete(this.players, playerIndex);
                };
                StartGameComponent.prototype.noPlayersSelected = function () {
                    for (var index = 0; index < this.players.length; index++) {
                        if (this.players[index].name) {
                            return false;
                        }
                    }
                    return true;
                };
                StartGameComponent.prototype.hasDuplicatePlayers = function () {
                    // Check there are no duplicates.
                    for (var outerIndex = 0; outerIndex < (this.players.length - 1); outerIndex++) {
                        if (this.players[outerIndex].name != null && this.players[outerIndex].name != '') {
                            for (var innerIndex = outerIndex + 1; innerIndex < this.players.length; innerIndex++) {
                                if (this.players[outerIndex].name == this.players[innerIndex].name) {
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                };
                StartGameComponent.prototype.startGame = function () {
                    console.log('Start game was pressed...');
                    console.log('Selected game config: ', this.selectedGameConfig);
                    this.gameService.configureAndInitialiseGame(this.selectedGameConfig, this.players);
                    this.router.navigateByUrl('/game');
                };
                return StartGameComponent;
            }());
            StartGameComponent = __decorate([
                core_1.Component({
                    selector: 'my-startGame',
                    templateUrl: '../templates/start-game.html',
                    providers: [
                        player_service_1.PlayerService
                    ]
                }),
                __metadata("design:paramtypes", [player_service_1.PlayerService, game_service_1.GameService, router_1.Router])
            ], StartGameComponent);
            exports_1("StartGameComponent", StartGameComponent);
        }
    };
});
//# sourceMappingURL=start-game.component.js.map