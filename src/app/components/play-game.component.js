System.register(["@angular/core", "@angular/router", "ng2-bootstrap", "./../services/game-service", "../enums/word-direction"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, ng2_bootstrap_1, game_service_1, word_direction_1, PlayGameComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (ng2_bootstrap_1_1) {
                ng2_bootstrap_1 = ng2_bootstrap_1_1;
            },
            function (game_service_1_1) {
                game_service_1 = game_service_1_1;
            },
            function (word_direction_1_1) {
                word_direction_1 = word_direction_1_1;
            }
        ],
        execute: function () {
            PlayGameComponent = (function () {
                function PlayGameComponent(gameService, router) {
                    this.gameService = gameService;
                    this.router = router;
                    this.modalConfig = {
                        ignoreBackdropClick: true,
                        keyboard: false,
                        focus: false
                    };
                    this.lookedUpWord = null;
                    this.errorMsg = null;
                    console.log('Game play - service is:', this.gameService);
                    this.gameState = gameService.getGameState();
                    console.log('Game play - state is:', this.gameState);
                    console.log('Game play - board from state is:', this.gameState.getBoard());
                    if (!this.gameState.isInProgress()) {
                        console.log('Game is not in progress - redirecting to home page');
                        this.router.navigateByUrl('/');
                    }
                }
                PlayGameComponent.prototype.keypress = function (e) {
                    console.log('Keyboard event: ', e);
                    // Pressing a key means different things at different times (and often nothing at all).
                    // Different entities know how to handle it, so this one tries to, and if it can't, passes it
                    // on to the game state.
                    var keyToUpper = e.key.toUpperCase();
                    if (e.keyCode == 27) {
                        // They pressed ESC.
                        if (this.isPlayerPlacingTiles()) {
                            this.cancelPlayTiles();
                        }
                        else if (this.isPlayerExchangingTiles()) {
                            this.cancelExchange();
                        }
                        else if (this.directionModal.isShown) {
                            this.hideDirectionModal();
                        }
                    }
                    else if (e.keyCode == 13) {
                        // They pressed RETURN.
                        if (this.isPlayerPlacingTiles() && this.canConfirmPlayTiles()) {
                            this.confirmPlayTiles();
                        }
                        else if (this.isPlayerExchangingTiles() && this.canConfirmExchange()) {
                            this.confirmExchange();
                        }
                    }
                    else if (e.keyCode == 8 || e.keyCode == 46) {
                        // The pressed BACKSPACE or DEL.
                        if (this.isPlayerPlacingTiles() && this.canRemoveLastTile()) {
                            this.removeLastTile();
                        }
                    }
                    else if (this.chooseLetterModal.isShown) {
                        // The "Blank letter" chooser is open, so that will handle the keypress.
                        this.selectLetter(keyToUpper);
                    }
                    else if (this.directionModal.isShown) {
                        // The direction chooser is open. See if they presses 'a' or 'd'.
                        if (keyToUpper == 'A') {
                            this.playAcross();
                        }
                        else if (keyToUpper == 'D') {
                            this.playDown();
                        }
                    }
                    else if (this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles()) {
                        var tileRackTiles = this.gameState.currentPlayer().getTileRack();
                        for (var index = 0; index < tileRackTiles.length; index++) {
                            if (tileRackTiles[index].getTile().getLetter() == keyToUpper) {
                                if (!tileRackTiles[index].getHasBeenSelected()) {
                                    // This handles whether they are placing tiles or exchanging them.
                                    this.selectTile(index);
                                    break;
                                }
                            }
                        }
                    }
                    else if (keyToUpper == 'S' && this.canPlayerShuffle()) {
                        this.playerShuffle();
                    }
                    else if (keyToUpper == 'P' && this.canPlayerPass()) {
                        this.playerPass();
                    }
                    else if (keyToUpper == 'X' && this.canPlayerExchange()) {
                        this.playerExchange();
                    }
                };
                PlayGameComponent.prototype.playerShuffle = function () {
                    this.gameState.currentPlayer().shuffleRack();
                };
                PlayGameComponent.prototype.playerPass = function () {
                    this.gameState.pass();
                    if (this.gameState.isGameOver()) {
                        this.endOfGameModal.config = this.modalConfig;
                        this.endOfGameModal.show();
                    }
                };
                PlayGameComponent.prototype.playerExchange = function () {
                    this.gameState.exchange();
                };
                PlayGameComponent.prototype.cellClick = function (boardRowCell) {
                    // If they're playing tiles or exchanging tiles or the game is over, just do nothing.
                    if (this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() || this.gameIsOver()) {
                        return;
                    }
                    this.startClickedCell = boardRowCell;
                    if (this.gameState.startWordPositionIsValid(boardRowCell)) {
                        // See if they are restricted to one direction or the other. If so, don't ask them!
                        var wordDirections = this.gameState.getAllowedDirectionsFromCell(boardRowCell);
                        switch (wordDirections.length) {
                            case 0:
                                alert('You cannot start a word from here!');
                                break;
                            case 1:
                                // Only 1 direction allowed, so choose it!
                                this.playDirection(wordDirections[0]);
                                break;
                            default:
                                // Multiple directions (ie across and down) allowed, so ask user which one they want.
                                this.directionModal.config = this.modalConfig;
                                this.directionModal.show();
                                break;
                        }
                    }
                    else {
                        alert('Sorry, you can\'t start a word there in this turn');
                    }
                };
                PlayGameComponent.prototype.playDirection = function (wordDirection) {
                    this.gameState.startPlayingTiles(this.startClickedCell, wordDirection);
                };
                PlayGameComponent.prototype.hideDirectionModal = function () {
                    this.directionModal.hide();
                };
                PlayGameComponent.prototype.cancelChooseLetterModal = function () {
                    // Remove the last tile played, which will be the blank tile with no letter.
                    this.removeLastTile();
                    this.chooseLetterModal.hide();
                };
                PlayGameComponent.prototype.acceptChooseLetterModal = function () {
                    this.chooseLetterModal.hide();
                };
                PlayGameComponent.prototype.playAcross = function () {
                    this.hideDirectionModal();
                    this.playDirection(word_direction_1.WordDirection.ACROSS);
                };
                PlayGameComponent.prototype.playDown = function () {
                    this.hideDirectionModal();
                    this.playDirection(word_direction_1.WordDirection.DOWN);
                };
                PlayGameComponent.prototype.selectTile = function (tileRackTileIndex) {
                    if (this.gameState.canSelectATile()) {
                        if (!this.gameState.currentPlayer().hasTileBeenSelected(tileRackTileIndex)) {
                            if (this.gameState.playerTurn.isInProgress()) {
                                if (this.gameState.playTile(tileRackTileIndex)) {
                                    // If the user just played a blank, ask them which letter they want it to be.
                                    var tileRackTile = this.gameState.currentPlayer().getTileRack()[tileRackTileIndex];
                                    if (tileRackTile.getTile().isBlank()) {
                                        this.gameState.setLastPlayedBlank(tileRackTile.getTile());
                                        this.chooseLetterModal.config = this.modalConfig;
                                        this.chooseLetterModal.show();
                                    }
                                }
                            }
                            else {
                                if (this.gameState.exchangeTiles.isInProgress()) {
                                    this.gameState.exchangeOneTile(tileRackTileIndex);
                                }
                            }
                        }
                    }
                };
                PlayGameComponent.prototype.isPlayerPlacingTiles = function () {
                    return this.gameState.playerTurn.isInProgress();
                };
                PlayGameComponent.prototype.removeLastTile = function () {
                    if (this.tilesHaveBeenPlayed()) {
                        this.gameState.removeLastTile();
                    }
                };
                PlayGameComponent.prototype.centreSquareHasTile = function () {
                    return this.gameState.getBoard().hasTileInCentre();
                };
                PlayGameComponent.prototype.tilesHaveBeenPlayed = function () {
                    return this.gameState.tilesHaveBeenPlayed();
                };
                PlayGameComponent.prototype.cancelPlayTiles = function () {
                    this.gameState.cancelPlayTiles();
                };
                PlayGameComponent.prototype.confirmPlayTiles = function () {
                    if (this.tilesHaveBeenPlayed() && this.centreSquareHasTile() && this.isTouchingExistingTile() && !this.hasSingleLetterWord()) {
                        this.gameState.confirmPlayTiles();
                        if (this.gameState.isGameOver()) {
                            this.endOfGameModal.config = this.modalConfig;
                            this.endOfGameModal.show();
                        }
                    }
                };
                PlayGameComponent.prototype.isTouchingExistingTile = function () {
                    return this.gameState.isTouchingExistingTile();
                };
                PlayGameComponent.prototype.hasSingleLetterWord = function () {
                    return this.gameState.hasSingleLetterWord();
                };
                PlayGameComponent.prototype.isFirstMove = function () {
                    return this.gameState.isFirstMove();
                };
                PlayGameComponent.prototype.selectLetter = function (whichLetter) {
                    if (this.gameState.selectLetter(whichLetter)) {
                        this.acceptChooseLetterModal();
                    }
                };
                PlayGameComponent.prototype.isPlayerExchangingTiles = function () {
                    return this.gameState.exchangeTiles.isInProgress();
                };
                PlayGameComponent.prototype.unexchangeTile = function (exchangeTilesIndex) {
                    this.gameState.unexchangeTiles(exchangeTilesIndex);
                };
                PlayGameComponent.prototype.cancelExchange = function () {
                    this.gameState.unexchangeAllTiles();
                };
                PlayGameComponent.prototype.confirmExchange = function () {
                    this.gameState.confirmExchange();
                };
                PlayGameComponent.prototype.enoughTilesInBag = function () {
                    return this.gameState.enoughTilesInBag();
                };
                PlayGameComponent.prototype.closeEndOfGameModal = function () {
                    this.endOfGameModal.hide();
                };
                PlayGameComponent.prototype.gameIsOver = function () {
                    return this.gameState.isEndOfGame();
                };
                PlayGameComponent.prototype.newGame = function () {
                    this.router.navigateByUrl('/startGame');
                };
                PlayGameComponent.prototype.replayGame = function () {
                    // Start the game again, using the same config as before.
                    this.gameService.initialiseGame();
                };
                PlayGameComponent.prototype.checkAbandonGame = function () {
                    return window.confirm('Are you sure?');
                };
                PlayGameComponent.prototype.canPlayerShuffle = function () {
                    return !this.gameIsOver();
                };
                PlayGameComponent.prototype.canPlayerPass = function () {
                    return !(this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() || this.gameIsOver());
                };
                PlayGameComponent.prototype.canPlayerExchange = function () {
                    return !(this.isPlayerPlacingTiles() || this.isPlayerExchangingTiles() || this.gameIsOver());
                };
                PlayGameComponent.prototype.canRemoveLastTile = function () {
                    return this.tilesHaveBeenPlayed();
                };
                PlayGameComponent.prototype.canConfirmPlayTiles = function () {
                    return this.tilesHaveBeenPlayed() && this.centreSquareHasTile() && this.isTouchingExistingTile() && !this.hasSingleLetterWord();
                };
                PlayGameComponent.prototype.canConfirmExchange = function () {
                    return this.gameState.exchangeTiles.hasTiles();
                };
                return PlayGameComponent;
            }());
            __decorate([
                core_1.ViewChild('directionModal'),
                __metadata("design:type", ng2_bootstrap_1.ModalDirective)
            ], PlayGameComponent.prototype, "directionModal", void 0);
            __decorate([
                core_1.ViewChild('chooseLetterModal'),
                __metadata("design:type", ng2_bootstrap_1.ModalDirective)
            ], PlayGameComponent.prototype, "chooseLetterModal", void 0);
            __decorate([
                core_1.ViewChild('endOfGameModal'),
                __metadata("design:type", ng2_bootstrap_1.ModalDirective)
            ], PlayGameComponent.prototype, "endOfGameModal", void 0);
            __decorate([
                core_1.HostListener('document:keypress', ['$event']),
                __metadata("design:type", Function),
                __metadata("design:paramtypes", [KeyboardEvent]),
                __metadata("design:returntype", void 0)
            ], PlayGameComponent.prototype, "keypress", null);
            __decorate([
                core_1.ViewChild('modal'),
                __metadata("design:type", Object)
            ], PlayGameComponent.prototype, "modal", void 0);
            PlayGameComponent = __decorate([
                core_1.Component({
                    selector: 'play-game-selector',
                    templateUrl: '../templates/play-game.html'
                }),
                __metadata("design:paramtypes", [game_service_1.GameService,
                    router_1.Router])
            ], PlayGameComponent);
            exports_1("PlayGameComponent", PlayGameComponent);
        }
    };
});
//# sourceMappingURL=play-game.component.js.map