import {Component} from '@angular/core';
import {RouterLink} from '@angular/router';

@Component({
  selector: 'welcome',
  templateUrl: '../templates/welcome.html',
  imports: [
    RouterLink
  ],
  providers: []
})
export class WelcomeComponent { }
