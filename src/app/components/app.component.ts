import {Component, ViewContainerRef} from '@angular/core';
import {GameService} from "../services/game-service";

// Add the RxJS Observable operators.
import './../rxjs-operators';
import {RouterOutlet} from '@angular/router';

@Component({
  selector: 'my-app',
  template: `
    <router-outlet></router-outlet>`,
  imports: [
    RouterOutlet
  ],
  providers: [
    GameService
  ]
})
export class AppComponent {
    constructor(private viewContainerRef: ViewContainerRef) {
        // You need this small hack in order to catch application root view container ref
        this.viewContainerRef = viewContainerRef;
    }
}
