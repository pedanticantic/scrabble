import {Component, Input} from '@angular/core';
import {Tile} from "../models/tile";
import {TileRackTile} from "../models/tile-rack-tile";
import {BoardRowCell} from "../models/board-row-cell";
import {NgClass} from '@angular/common';

@Component({
  selector: 'tile-container',
  imports: [
    NgClass
  ],
  templateUrl: '../templates/one-tile.html'
})
export class TileComponent {
    @Input() tile: Tile;
    @Input() tileRackTile: TileRackTile = null;
    @Input() boardRowCell: BoardRowCell = null;
    @Input() showLetterIfBlank: string = '0';

    public getExtraClasses(): string {
        let result = [];
        if (this.tileRackTile != null) {
            result.push(this.tileRackTile.getCssSubclass());
            if (this.tileRackTile.getTile().isBlank()) {
                result.push('blank-tile');
            }
        }
        if (this.boardRowCell != null) {
            result.push(this.boardRowCell.getCssSubclass());
            if (this.boardRowCell.getTile().isBlank()) {
                result.push('blank-tile');
            }
        }

        return result.join(' ');
    }
}
