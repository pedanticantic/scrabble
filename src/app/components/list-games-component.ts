import {Component, OnInit} from "@angular/core";
import {GameStoreService} from "../services/game-store.service";
import {Router} from "@angular/router";
import {WordLookupService} from "../services/word-lookup.service";
import {GameService} from "../services/game-service";
import {NgForOf} from '@angular/common';

@Component({
  selector: 'my-listGames',
  templateUrl: '../templates/list-games.html',
  imports: [
    NgForOf
  ]
})
export class ListGamesComponent implements OnInit {
    public games: any[] = [];

    constructor(
        private router: Router,
        private gameStoreService: GameStoreService,
        private wordLookupService: WordLookupService,
        private gameService: GameService
    ) {}

    ngOnInit(): void {
        this.loadGames()
    }

    public deleteGame(game: any): void {
        if (confirm('Are you sure you want to delete '+game.name+'?')) {
            GameStoreService.removeGame(game.id);
            this.loadGames();
        }
    }

    public loadGames(): void {
        this.games = [];
        let gameIDsList = GameStoreService.getGamesIdList();
        for (let gameIdx = 0 ; gameIdx < gameIDsList.length ; gameIdx++) {
            let game = GameStoreService.loadGameInitialState(gameIDsList[gameIdx]);
            this.games.push({
                id: gameIDsList[gameIdx],
                name: game.name,
                playerCount: game.hasOwnProperty('players') ? game.players.length : 0
            })
        }
    }

    public restoreGame(game: any): void {
        let gameState = this.gameStoreService.restoreGame(game.id, this.wordLookupService, this.gameStoreService);
        this.gameService.setGameState(gameState);
        this.router.navigateByUrl('/game');
    }
}
