System.register(["@angular/core", "@angular/platform-browser", "@angular/forms", "@angular/router", "ng2-bootstrap/ng2-bootstrap", "@angular/http", "./components/app.component", "./services/player.service", "./components/start-game.component", "./components/welcome-component", "./components/play-game.component", "./components/tile.componant", "./services/word-lookup.service", "./services/game-service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, platform_browser_1, forms_1, router_1, ng2_bootstrap_1, ng2_bootstrap_2, http_1, app_component_1, player_service_1, start_game_component_1, welcome_component_1, play_game_component_1, tile_componant_1, word_lookup_service_1, game_service_1, AppModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (ng2_bootstrap_1_1) {
                ng2_bootstrap_1 = ng2_bootstrap_1_1;
                ng2_bootstrap_2 = ng2_bootstrap_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (player_service_1_1) {
                player_service_1 = player_service_1_1;
            },
            function (start_game_component_1_1) {
                start_game_component_1 = start_game_component_1_1;
            },
            function (welcome_component_1_1) {
                welcome_component_1 = welcome_component_1_1;
            },
            function (play_game_component_1_1) {
                play_game_component_1 = play_game_component_1_1;
            },
            function (tile_componant_1_1) {
                tile_componant_1 = tile_componant_1_1;
            },
            function (word_lookup_service_1_1) {
                word_lookup_service_1 = word_lookup_service_1_1;
            },
            function (game_service_1_1) {
                game_service_1 = game_service_1_1;
            }
        ],
        execute: function () {
            AppModule = (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = __decorate([
                core_1.NgModule({
                    imports: [
                        platform_browser_1.BrowserModule,
                        forms_1.FormsModule,
                        ng2_bootstrap_1.Ng2BootstrapModule,
                        ng2_bootstrap_2.ModalModule,
                        router_1.RouterModule.forRoot([
                            {
                                path: '',
                                component: welcome_component_1.WelcomeComponent
                            },
                            {
                                path: 'startGame',
                                component: start_game_component_1.StartGameComponent
                            },
                            {
                                path: 'game',
                                component: play_game_component_1.PlayGameComponent
                            }
                        ]),
                        http_1.HttpModule,
                        http_1.JsonpModule
                    ],
                    declarations: [
                        app_component_1.AppComponent,
                        start_game_component_1.StartGameComponent,
                        welcome_component_1.WelcomeComponent,
                        play_game_component_1.PlayGameComponent,
                        tile_componant_1.TileComponent
                    ],
                    providers: [
                        player_service_1.PlayerService,
                        game_service_1.GameService,
                        word_lookup_service_1.WordLookupService
                    ],
                    bootstrap: [
                        app_component_1.AppComponent
                    ]
                }),
                __metadata("design:paramtypes", [])
            ], AppModule);
            exports_1("AppModule", AppModule);
        }
    };
});
//# sourceMappingURL=app.module.js.map