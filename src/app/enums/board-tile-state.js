System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var BoardTileState;
    return {
        setters: [],
        execute: function () {
            (function (BoardTileState) {
                BoardTileState[BoardTileState["IN_TURN"] = 0] = "IN_TURN";
                BoardTileState[BoardTileState["JUST_PLAYED"] = 1] = "JUST_PLAYED";
                BoardTileState[BoardTileState["CONFIRMED"] = 2] = "CONFIRMED";
            })(BoardTileState || (BoardTileState = {}));
            exports_1("BoardTileState", BoardTileState);
        }
    };
});
//# sourceMappingURL=board-tile-state.js.map