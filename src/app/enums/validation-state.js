System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ValidationState;
    return {
        setters: [],
        execute: function () {
            (function (ValidationState) {
                ValidationState[ValidationState["IN_PROGRESS"] = 0] = "IN_PROGRESS";
                ValidationState[ValidationState["VALID"] = 1] = "VALID";
                ValidationState[ValidationState["NOT_VALID"] = 2] = "NOT_VALID";
            })(ValidationState || (ValidationState = {}));
            exports_1("ValidationState", ValidationState);
        }
    };
});
//# sourceMappingURL=validation-state.js.map