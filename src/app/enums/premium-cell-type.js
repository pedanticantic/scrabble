System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var PremiumCellType;
    return {
        setters: [],
        execute: function () {
            (function (PremiumCellType) {
                PremiumCellType[PremiumCellType["DLS"] = 0] = "DLS";
                PremiumCellType[PremiumCellType["TLS"] = 1] = "TLS";
                PremiumCellType[PremiumCellType["QLS"] = 2] = "QLS";
                PremiumCellType[PremiumCellType["DWS"] = 3] = "DWS";
                PremiumCellType[PremiumCellType["TWS"] = 4] = "TWS";
                PremiumCellType[PremiumCellType["QWS"] = 5] = "QWS";
            })(PremiumCellType || (PremiumCellType = {}));
            exports_1("PremiumCellType", PremiumCellType);
        }
    };
});
//# sourceMappingURL=premium-cell-type.js.map