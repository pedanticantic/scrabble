System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var WordDirection;
    return {
        setters: [],
        execute: function () {
            (function (WordDirection) {
                WordDirection[WordDirection["ACROSS"] = 0] = "ACROSS";
                WordDirection[WordDirection["DOWN"] = 1] = "DOWN";
            })(WordDirection || (WordDirection = {}));
            exports_1("WordDirection", WordDirection);
        }
    };
});
//# sourceMappingURL=word-direction.js.map