import {ValidationState} from "../enums/validation-state";

export class WordLookupResponse {
    constructor (
        private valid: ValidationState,
        private definitions: string[] = []
    ) {}

    public static buildResponse(rawResponse?: any): WordLookupResponse {
        return (rawResponse.length > 0)
            ?
            new WordLookupResponse(
                ValidationState.VALID,
                rawResponse.map(
                    (definition) => {
                        return definition.hasOwnProperty('text') ? definition.text : 'Unknown';
                    }
                )
            )
            :
            new WordLookupResponse(ValidationState.NOT_VALID);
    }

    public getValidState(): ValidationState {
        return this.valid;
    }

    public getDefinitions(): string[] {
        return this.definitions;
    }
}