System.register(["./played-word", "./cell-coordinates", "../enums/word-direction", "../enums/board-tile-state"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var played_word_1, cell_coordinates_1, word_direction_1, board_tile_state_1, WordsFormed;
    return {
        setters: [
            function (played_word_1_1) {
                played_word_1 = played_word_1_1;
            },
            function (cell_coordinates_1_1) {
                cell_coordinates_1 = cell_coordinates_1_1;
            },
            function (word_direction_1_1) {
                word_direction_1 = word_direction_1_1;
            },
            function (board_tile_state_1_1) {
                board_tile_state_1 = board_tile_state_1_1;
            }
        ],
        execute: function () {
            WordsFormed = (function () {
                function WordsFormed(wordLookupService) {
                    this.wordLookupService = wordLookupService;
                    this.words = [];
                    this.bonuses = [];
                }
                WordsFormed.prototype.addWord = function (board, playerTurn, startCoordinate, wordDirection, isMainWord) {
                    if (isMainWord === void 0) { isMainWord = true; }
                    var word = '';
                    var wordScore = 0;
                    var scoreMultiplier = 1;
                    var currentCoordinate = startCoordinate;
                    while (board.hasTileAt(currentCoordinate)) {
                        // Process the letter and tile score
                        var placedTile = board.getTileAt(currentCoordinate);
                        word += placedTile.getPlacedLetter(false, '1');
                        wordScore += placedTile.getPointsValue() * board.getTileMultiplierAt(currentCoordinate);
                        scoreMultiplier *= board.getWordMultiplierAt(currentCoordinate);
                        // See if we're looking at the main word and this is a tile placed in this turn and
                        // there's an adjoining word.
                        if (isMainWord && board.getTileStateAt(currentCoordinate) == board_tile_state_1.BoardTileState.IN_TURN) {
                            var deltaRow = (wordDirection == word_direction_1.WordDirection.DOWN ? 0 : 1);
                            var deltaCol = (wordDirection == word_direction_1.WordDirection.ACROSS ? 0 : 1);
                            var firstSide = new cell_coordinates_1.CellCoordinates(currentCoordinate.getRow() + deltaRow, currentCoordinate.getCol() + deltaCol);
                            var secondSide = new cell_coordinates_1.CellCoordinates(currentCoordinate.getRow() - deltaRow, currentCoordinate.getCol() - deltaCol);
                            if (board.hasTileAt(firstSide) || board.hasTileAt(secondSide)) {
                                // Yes, newly played tile is touching an adjacent tile. Find the start of the word.
                                // @TODO: Make this less convoluted!
                                var adjoiningWordStart = new cell_coordinates_1.CellCoordinates(currentCoordinate.getRow(), currentCoordinate.getCol());
                                do {
                                    adjoiningWordStart = new cell_coordinates_1.CellCoordinates(adjoiningWordStart.getRow() - deltaRow, adjoiningWordStart.getCol() - deltaCol);
                                } while (board.hasTileAt(adjoiningWordStart));
                                adjoiningWordStart = new cell_coordinates_1.CellCoordinates(adjoiningWordStart.getRow() + deltaRow, adjoiningWordStart.getCol() + deltaCol);
                                this.addWord(board, playerTurn, adjoiningWordStart, (wordDirection == word_direction_1.WordDirection.DOWN ? word_direction_1.WordDirection.ACROSS : word_direction_1.WordDirection.DOWN), false);
                            }
                        }
                        // Move to next cell.
                        currentCoordinate = new cell_coordinates_1.CellCoordinates(currentCoordinate.getRow() + (wordDirection == word_direction_1.WordDirection.DOWN ? 1 : 0), currentCoordinate.getCol() + (wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                    }
                    // Multiply the word score by the word score multiplier
                    // (any double word and/or triple word score cells).
                    wordScore *= scoreMultiplier;
                    wordScore = word.length <= 1 ? 0 : wordScore;
                    // Add the word to the list.
                    var newPlayedWord = new played_word_1.PlayedWord(word, wordScore);
                    if (isMainWord) {
                        this.words.unshift(newPlayedWord); // We want to add the main word to the start of the list.
                    }
                    else {
                        this.words.push(newPlayedWord);
                    }
                };
                WordsFormed.prototype.update = function (board, playerTurn) {
                    // Work out all the words formed.
                    this.words = [];
                    // The main word that was played - start at the initial position and look at all contiguous
                    // letters in the appropriate direction. Sort out all the premium squares as we go. Also,
                    // determine any adjoining words that have been formed/modified.
                    var initialPosition = playerTurn.getInitialPosition();
                    var wordDirection = playerTurn.getWordDirection();
                    // @TODO: This could be a bit nicer.
                    while (board.hasTileAt(new cell_coordinates_1.CellCoordinates(initialPosition.getRow() - (wordDirection == word_direction_1.WordDirection.ACROSS ? 0 : 1), initialPosition.getCol() - (wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0)))) {
                        initialPosition = new cell_coordinates_1.CellCoordinates(initialPosition.getRow() - (wordDirection == word_direction_1.WordDirection.ACROSS ? 0 : 1), initialPosition.getCol() - (wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                    }
                    this.addWord(board, playerTurn, initialPosition, wordDirection);
                    // Work out any bonuses.
                    this.bonuses = [];
                    if (playerTurn.hasPlayedAllTiles()) {
                        this.bonuses.push(new played_word_1.PlayedWord('Bonus', 50));
                    }
                    this.totalScore = 0;
                    for (var wordIndex = 0; wordIndex < this.words.length; wordIndex++) {
                        this.totalScore += this.words[wordIndex].getScore();
                    }
                    for (var bonusIndex = 0; bonusIndex < this.bonuses.length; bonusIndex++) {
                        this.totalScore += this.bonuses[bonusIndex].getScore();
                    }
                    // Look up the words in the online dictionary.
                    // @TODO: Make this better! Improve the way the word lookup service is injected.
                    var self = this;
                    this.words.forEach(function (word) {
                        self.wordLookupService.validateWord(word);
                    });
                };
                WordsFormed.prototype.getWords = function () {
                    return this.words;
                };
                WordsFormed.prototype.getBonuses = function () {
                    return this.bonuses;
                };
                WordsFormed.prototype.getScore = function () {
                    return this.totalScore;
                };
                WordsFormed.prototype.hasSingleLetterWord = function () {
                    for (var wordIndex = 0; wordIndex < this.words.length; wordIndex++) {
                        if (this.words[wordIndex].getWordLength() == 1) {
                            return true;
                        }
                    }
                    return false;
                };
                return WordsFormed;
            }());
            exports_1("WordsFormed", WordsFormed);
        }
    };
});
//# sourceMappingURL=words-formed.js.map