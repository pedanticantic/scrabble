// Represents a row of the board during a game.
import {Tile} from './tile';
import {CellCoordinates} from "./cell-coordinates";
import {BoardTileState} from "../enums/board-tile-state";
import {PremiumCellType} from "../enums/premium-cell-type";

export class BoardRowCell {
    private cellType: PremiumCellType = null;
    private placedTile: Tile = null;
    private tileState: BoardTileState = null;

    private className: string = 'none'; // The default cell colour.
    private cellTooltip: string = null;
    private cellText: string = null;

    constructor(private cellCoordinates: CellCoordinates) {
        this.cellCoordinates = cellCoordinates;
    }

    public setPremiumCell(premiumCellType: PremiumCellType, isCentre: boolean) {
        this.cellType = premiumCellType;

        // Set other attributes now that we know the cell type.
        switch (this.cellType) {
            case PremiumCellType.DLS:
                this.className = 'dls';
                this.cellTooltip = 'Double Letter Score';
                this.cellText = 'DL';
                break;
            case PremiumCellType.TLS:
                this.className = 'tls';
                this.cellTooltip = 'Triple Letter Score';
                this.cellText = 'TL';
                break;
            case PremiumCellType.QLS:
                this.className = 'qls';
                this.cellTooltip = 'Quadruple Letter Score';
                this.cellText = 'QL';
                break;
            case PremiumCellType.DWS:
                this.className = 'dws';
                this.cellTooltip = 'Double Word Score';
                this.cellText = 'DW';
                break;
            case PremiumCellType.TWS:
                this.className = 'tws';
                this.cellTooltip = 'Triple Word Score';
                this.cellText = 'TW';
                break;
            case PremiumCellType.QWS:
                this.className = 'qws';
                this.cellTooltip = 'Quadruple Word Score';
                this.cellText = 'QW';
                break;
        }
        if (isCentre && this.cellText != null) {
            this.cellText = '*';
        }
    }

    public getPremiumType(): PremiumCellType {
        return this.cellType;
    }

    public getClassName(): string {
        return this.className;
    }

    public getCellTooltip(): string {
        return this.cellTooltip;
    }

    public getCellText(): string {
        return this.cellText;
    }

    public getCellCoordinates(): CellCoordinates {
        return this.cellCoordinates;
    }

    public getTile(): Tile {
        return this.placedTile;
    }

    public getTileState(): BoardTileState {
        return this.tileState;
    }

    public setTile(playedTile: Tile, newState?: BoardTileState): void {
        this.placedTile = playedTile;
        this.tileState = newState ? newState : BoardTileState.IN_TURN;
    }

    public hasTile(): boolean {
        return !(this.placedTile == null);
    }

    public clearTile(): void {
        if (this.placedTile != null) {
            this.placedTile.unsetLetterForBlank();
        }
        this.placedTile = null;
        this.tileState = null;
    }

    public wordIsConfirmed(): void {
        // Transform the states as follows:
        // JUST_PLAYED => CONFIRMED
        // IN_TURN => JUST_PLAYED
        switch (this.tileState) {
            case BoardTileState.JUST_PLAYED:
                this.tileState = BoardTileState.CONFIRMED;
                break;
            case BoardTileState.IN_TURN:
                this.tileState = BoardTileState.JUST_PLAYED;
                break;
            default:
                break;
        }
    }

    public getCssSubclass(): string {
        let cssSubClass = '';
        switch (this.tileState) {
            case BoardTileState.IN_TURN:
                cssSubClass = 'in-turn';
                break;
            case BoardTileState.JUST_PLAYED:
                cssSubClass = 'just-played';
                break;
            case BoardTileState.CONFIRMED:
                cssSubClass = 'confirmed';
                break;
            default:
                break;
        }

        return cssSubClass;
    }
}
