// This class represents a tile in a player's tile rack.
import {Tile} from "./tile";

export class TileRackTile {
    private tile: Tile;
    private hasBeenSelected: boolean = false; // Selected for whatever reason.

    constructor(tile: Tile) {
        this.tile = tile;
    }

    public getTile(): Tile {
        return this.tile;
    }

    public getCssSubclass(): string {
        return this.hasBeenSelected ? 'selected' : '';
    }

    public getHasBeenSelected(): boolean {
        return this.hasBeenSelected;
    }

    public setSelected(): void {
        this.hasBeenSelected = true;
    }

    public unsetSelected(): void {
        this.hasBeenSelected = false;
    }
}