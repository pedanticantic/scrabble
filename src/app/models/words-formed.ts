// This class represents all the words and bonuses generated in this go.
import {Board} from "./board";
import {PlayerTurn} from "./player-turn";
import {PlayedWord} from "./played-word";
import {CellCoordinates} from "./cell-coordinates";
import {WordDirection} from "../enums/word-direction";
import {BoardTileState} from "../enums/board-tile-state";
import {WordLookupService} from "../services/word-lookup.service";

export class WordsFormed {
    private words: PlayedWord[] = [];
    private bonuses: PlayedWord[] = [];
    private totalScore: number;

    constructor(private wordLookupService: WordLookupService) { }

    private addWord(board: Board, playerTurn: PlayerTurn, startCoordinate: CellCoordinates, wordDirection: WordDirection, isMainWord: boolean = true): void {
        let word: string = '';
        let wordScore: number = 0;
        let scoreMultiplier: number = 1;
        let currentCoordinate = startCoordinate;
        while (board.hasTileAt(currentCoordinate)) {
            // Process the letter and tile score
            let placedTile = board.getTileAt(currentCoordinate);
            word += placedTile.getPlacedLetter(false, '1');
            wordScore += placedTile.getPointsValue() * board.getTileMultiplierAt(currentCoordinate);
            scoreMultiplier *= board.getWordMultiplierAt(currentCoordinate);
            // See if we're looking at the main word and this is a tile placed in this turn and
            // there's an adjoining word.
            if (isMainWord && board.getTileStateAt(currentCoordinate) == BoardTileState.IN_TURN) {
                let deltaRow = (wordDirection == WordDirection.DOWN ? 0 : 1);
                let deltaCol = (wordDirection == WordDirection.ACROSS ? 0 : 1);
                let firstSide = new CellCoordinates(
                    currentCoordinate.getRow() + deltaRow, currentCoordinate.getCol() + deltaCol
                );
                let secondSide = new CellCoordinates(
                    currentCoordinate.getRow() - deltaRow, currentCoordinate.getCol() - deltaCol
                );
                if (board.hasTileAt(firstSide) || board.hasTileAt(secondSide)) {
                    // Yes, newly played tile is touching an adjacent tile. Find the start of the word.
                    // @TODO: Make this less convoluted!
                    let adjoiningWordStart = new CellCoordinates(currentCoordinate.getRow(), currentCoordinate.getCol());
                    do {
                        adjoiningWordStart = new CellCoordinates(adjoiningWordStart.getRow() - deltaRow, adjoiningWordStart.getCol() - deltaCol);
                    } while (board.hasTileAt(adjoiningWordStart));
                    adjoiningWordStart = new CellCoordinates(adjoiningWordStart.getRow() + deltaRow, adjoiningWordStart.getCol() + deltaCol);
                    this.addWord(board, playerTurn, adjoiningWordStart, (wordDirection == WordDirection.DOWN ? WordDirection.ACROSS : WordDirection.DOWN), false);
                }
            }
            // Move to next cell.
            currentCoordinate = new CellCoordinates(
                currentCoordinate.getRow() + (wordDirection == WordDirection.DOWN ? 1 : 0),
                currentCoordinate.getCol() + (wordDirection == WordDirection.ACROSS ? 1 : 0)
            );
        }
        // Multiply the word score by the word score multiplier
        // (any double word and/or triple word score cells).
        wordScore *= scoreMultiplier;
        wordScore = word.length <= 1 ? 0 : wordScore;
        // Add the word to the list.
        let newPlayedWord = new PlayedWord(word, wordScore);
        if (isMainWord) {
            this.words.unshift(newPlayedWord); // We want to add the main word to the start of the list.
        } else {
            this.words.push(newPlayedWord);
        }
    }

    public update(board: Board, playerTurn: PlayerTurn): void {
        // Work out all the words formed.
        this.words = [];
        // The main word that was played - start at the initial position and look at all contiguous
        // letters in the appropriate direction. Sort out all the premium squares as we go. Also,
        // determine any adjoining words that have been formed/modified.
        let initialPosition = playerTurn.getInitialPosition();
        let wordDirection = playerTurn.getWordDirection();
        // @TODO: This could be a bit nicer.
        while (board.hasTileAt(new CellCoordinates(
            initialPosition.getRow() - (wordDirection == WordDirection.ACROSS ? 0 : 1),
            initialPosition.getCol() - (wordDirection == WordDirection.ACROSS ? 1 : 0)
        ))) {
            initialPosition = new CellCoordinates(
                initialPosition.getRow() - (wordDirection == WordDirection.ACROSS ? 0 : 1),
                initialPosition.getCol() - (wordDirection == WordDirection.ACROSS ? 1 : 0)
            );
        }
        this.addWord(board, playerTurn, initialPosition, wordDirection);

        // Work out any bonuses.
        this.bonuses = [];
        if (playerTurn.hasPlayedAllTiles()) {
            this.bonuses.push(new PlayedWord('Bonus', 50));
        }

        this.totalScore = 0;
        for(let wordIndex = 0 ; wordIndex < this.words.length ; wordIndex++) {
            this.totalScore += this.words[wordIndex].getScore();
        }
        for(let bonusIndex = 0 ; bonusIndex < this.bonuses.length ; bonusIndex++) {
            this.totalScore += this.bonuses[bonusIndex].getScore();
        }

        // Look up the words in the online dictionary.
        // @TODO: Make this better! Improve the way the word lookup service is injected.
        let self = this;
        this.words.forEach(function(word: PlayedWord) {
            self.wordLookupService.validateWord(word);
        });

    }

    public getWords(): PlayedWord[] {
        return this.words;
    }

    public getBonuses(): PlayedWord[] {
        return this.bonuses;
    }

    public getScore(): number {
        return this.totalScore;
    }

    public hasSingleLetterWord(): boolean {
        for(let wordIndex = 0 ; wordIndex < this.words.length ; wordIndex++) {
            if (this.words[wordIndex].getWordLength() == 1) {
                return true;
            }
        }

        return false;
    }
}