System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Tile;
    return {
        setters: [],
        execute: function () {
            // Represents a single tile. The "letterIfBlank" gets populated when a player nominates
            // a letter if the tile is blank.
            Tile = (function () {
                function Tile(tileLetter, pointsValue) {
                    this.tileLetter = tileLetter;
                    this.pointsValue = pointsValue;
                    this.letterIfBlank = null;
                }
                ;
                Tile.prototype.getLetter = function () {
                    return this.tileLetter;
                };
                Tile.prototype.getPointsValue = function () {
                    return this.pointsValue;
                };
                Tile.prototype.getPlacedLetter = function (useNBSp, showLetterIfBlank) {
                    if (useNBSp === void 0) { useNBSp = false; }
                    if (showLetterIfBlank === void 0) { showLetterIfBlank = '0'; }
                    return this.isBlank() ? (this.letterIfBlank == null ? (useNBSp ? '&nbsp;' : '?') : (showLetterIfBlank == '1' ? this.letterIfBlank : '&nbsp;')) : this.tileLetter;
                };
                Tile.prototype.isBlank = function () {
                    return this.tileLetter == ' ';
                };
                Tile.prototype.setLetterForBlank = function (whichLetter) {
                    if (this.isBlank()) {
                        this.letterIfBlank = whichLetter;
                    }
                };
                Tile.prototype.unsetLetterForBlank = function () {
                    if (this.isBlank()) {
                        this.letterIfBlank = null;
                    }
                };
                return Tile;
            }());
            exports_1("Tile", Tile);
        }
    };
});
//# sourceMappingURL=tile.js.map