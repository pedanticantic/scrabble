System.register(["./../services/shuffler", "./tile-rack-tile"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var shuffler_1, tile_rack_tile_1, Player;
    return {
        setters: [
            function (shuffler_1_1) {
                shuffler_1 = shuffler_1_1;
            },
            function (tile_rack_tile_1_1) {
                tile_rack_tile_1 = tile_rack_tile_1_1;
            }
        ],
        execute: function () {
            Player = (function () {
                function Player(newName, newIsHuman, newComputerLevel) {
                    this.score = 0;
                    this.adjustmentAmount = 0;
                    this.shuffler = new shuffler_1.Shuffler();
                    this.numConsecutivePasses = 0;
                    this.hasWon = false;
                    this.name = newName;
                    this.isHuman = newIsHuman;
                    this.computerLevel = newComputerLevel;
                    this.tileRack = [];
                }
                Player.prototype.getName = function () {
                    return this.name;
                };
                Player.prototype.isRackFull = function () {
                    return this.tileRack.length >= 7;
                };
                Player.prototype.addTileToRack = function (tile) {
                    if (this.isRackFull()) {
                        console.log('Something it trying to overfill a rack!');
                    }
                    this.tileRack.push(new tile_rack_tile_1.TileRackTile(tile));
                };
                Player.prototype.getTileRack = function () {
                    return this.tileRack;
                };
                Player.prototype.getScore = function () {
                    return this.score;
                };
                Player.prototype.getScoreAdjustment = function () {
                    return this.adjustmentAmount;
                };
                Player.prototype.shuffleRack = function () {
                    this.shuffler.shuffle(this.tileRack);
                };
                Player.prototype.hasTileBeenSelected = function (tileRackTileIndex) {
                    // @TODO: We should to a range check on the passed in index.
                    return this.tileRack[tileRackTileIndex].getHasBeenSelected();
                };
                Player.prototype.unplayTile = function (removedTile) {
                    // We loop through the rack from right to left so that it unselects in a natural order.
                    for (var tileIndex = this.tileRack.length - 1; tileIndex >= 0; tileIndex--) {
                        if (removedTile.getLetter() == this.tileRack[tileIndex].getTile().getLetter()) {
                            if (this.tileRack[tileIndex].getHasBeenSelected()) {
                                this.tileRack[tileIndex].unsetSelected();
                                break;
                            }
                        }
                    }
                };
                Player.prototype.wordIsConfirmed = function (tileBag) {
                    // We need to remove any tiles played in the turn from the rack, and
                    // replenish them from the bag.
                    this.tileRack = this.tileRack.filter(function (tileRackTile) {
                        return !tileRackTile.getHasBeenSelected();
                    });
                    this.replenishFromTileBag(tileBag);
                };
                Player.prototype.replenishFromTileBag = function (tileBag) {
                    while (!this.isRackFull() && !tileBag.isEmpty()) {
                        this.addTileToRack(tileBag.takeTile());
                    }
                };
                /**
                 * Method to add the given number onto the player's score. The number can be
                 * negative (as at the end of the game).
                 * @param delta number
                 */
                Player.prototype.addToScore = function (delta) {
                    this.score += delta;
                };
                Player.prototype.exchangeSelectedTiles = function (tileBag) {
                    // Okay, split the rack into an array of tiles that were selected, and the remaining
                    // tile rack tile objects.
                    var extractedTiles = [];
                    var newTileRack = [];
                    this.tileRack.forEach(function (tileRackTile) {
                        if (tileRackTile.getHasBeenSelected()) {
                            extractedTiles.push(tileRackTile.getTile());
                        }
                        else {
                            newTileRack.push(tileRackTile);
                        }
                    });
                    this.tileRack = newTileRack;
                    // Now replenish the rack from the bag. It's important to do this before putting the
                    // selected tiles back into the bag, or they might get one of the same tiles back again.
                    this.replenishFromTileBag(tileBag);
                    // Now, put the selected tiles back into the bag. The bag knows to shuffle the tiles after.
                    tileBag.replaceTiles(extractedTiles);
                };
                Player.prototype.updatedPassesCount = function (wasAPass) {
                    if (wasAPass) {
                        this.numConsecutivePasses++;
                    }
                    else {
                        this.numConsecutivePasses = 0;
                    }
                };
                Player.prototype.hasPassedTwice = function () {
                    return this.numConsecutivePasses >= 2;
                };
                Player.prototype.hasNoTilesInRack = function () {
                    return this.tileRack.length == 0;
                };
                Player.prototype.calculateAdjustment = function () {
                    // We simply add up the scores on all the tiles in the rack, then subtract them
                    // from the player score, set the adjustment amount, and return the amount.
                    var adjustment = 0;
                    this.tileRack.forEach(function (tileRackTile) {
                        adjustment += tileRackTile.getTile().getPointsValue();
                    });
                    this.setScoreAdjustment(-1 * adjustment);
                    return adjustment;
                };
                Player.prototype.setScoreAdjustment = function (adjustmentAmount) {
                    this.score += adjustmentAmount;
                    this.adjustmentAmount = adjustmentAmount;
                };
                Player.prototype.testForVictory = function (highestScore) {
                    this.hasWon = (this.score == highestScore);
                };
                Player.prototype.isWinner = function () {
                    return this.hasWon;
                };
                Player.prototype.initialise = function (tileBag) {
                    this.tileRack = [];
                    this.replenishFromTileBag(tileBag);
                    this.score = 0;
                    this.adjustmentAmount = 0;
                    this.numConsecutivePasses = 0;
                    this.hasWon = false;
                };
                return Player;
            }());
            exports_1("Player", Player);
        }
    };
});
//# sourceMappingURL=player.js.map