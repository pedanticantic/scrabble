System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var TileRackTile;
    return {
        setters: [],
        execute: function () {
            TileRackTile = (function () {
                function TileRackTile(tile) {
                    this.hasBeenSelected = false; // Selected for whatever reason.
                    this.tile = tile;
                }
                TileRackTile.prototype.getTile = function () {
                    return this.tile;
                };
                TileRackTile.prototype.getCssSubclass = function () {
                    return this.hasBeenSelected ? 'selected' : '';
                };
                TileRackTile.prototype.getHasBeenSelected = function () {
                    return this.hasBeenSelected;
                };
                TileRackTile.prototype.setSelected = function () {
                    this.hasBeenSelected = true;
                };
                TileRackTile.prototype.unsetSelected = function () {
                    this.hasBeenSelected = false;
                };
                return TileRackTile;
            }());
            exports_1("TileRackTile", TileRackTile);
        }
    };
});
//# sourceMappingURL=tile-rack-tile.js.map