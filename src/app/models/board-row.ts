// Represents a row of the board during a game.
import {BoardRowCell} from './board-row-cell';
import {CellCoordinates} from "./cell-coordinates";
import {Tile} from "./tile";
import {BoardTileState} from "../enums/board-tile-state";
import {PremiumCellType} from "../enums/premium-cell-type";

export class BoardRow {
    boardRowCells: BoardRowCell[] = [];

    public addCell(boardRowCell: BoardRowCell) {
        this.boardRowCells.push(boardRowCell);
    }

    public setPremiumCell(premiumType: PremiumCellType, colIndex: number, isCentre: boolean) {
        this.boardRowCells[colIndex].setPremiumCell(premiumType, isCentre);
    }

    public getRowCells(): BoardRowCell[] {
        return this.boardRowCells;
    }

    public getTileAt(cellCoordinates: CellCoordinates): Tile {
        return this.boardRowCells[cellCoordinates.getCol()].getTile();
    }

    public getTileStateAt(cellCoordinates: CellCoordinates): BoardTileState {
        return this.boardRowCells[cellCoordinates.getCol()].getTileState();
    }

    public wordIsConfirmed(): void {
        for(let colIndex = 0 ; colIndex < this.boardRowCells.length ; colIndex++) {
            this.boardRowCells[colIndex].wordIsConfirmed();
        }
    }
}
