// Represents a bag of tiles during a game.
import {Tile} from './tile';
import {Shuffler} from '../services/shuffler';

export class TileBag {
    tiles: Tile[] = [];
    private shuffler: Shuffler = new Shuffler();

    public add(tile: Tile) {
        this.tiles.push(tile);
    }

    public isEmpty(): boolean {
        return this.tiles.length == 0;
    }

    public howManyTiles(): number {
        return this.tiles.length;
    }

    public shuffleBag() {
        this.shuffler.shuffle(this.tiles);
    }

    public takeTile(): Tile {
        if (this.isEmpty()) {
            console.log('Something is asking the tileBag for a tile, but it is empty!');
            return null;
        }

        return this.tiles.pop();
    }

    public replaceTiles(tiles: Tile[]): void {
        // Add the tiles to the end of the array of tiles.
        this.tiles = this.tiles.concat(tiles);
        // Shuffle the bag.
        this.shuffleBag();
    }

    public restoreState(previousState: any) {
        this.tiles = [];
        let self = this;
        previousState.forEach(function (tileInfo: any) {
            self.add(new Tile(tileInfo.letter, tileInfo.pointsValue));
        })
    }
}
