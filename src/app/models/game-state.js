System.register(["./cell-coordinates", "../enums/word-direction", "./player-turn", "./exchange-tiles"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var cell_coordinates_1, word_direction_1, player_turn_1, exchange_tiles_1, GameState;
    return {
        setters: [
            function (cell_coordinates_1_1) {
                cell_coordinates_1 = cell_coordinates_1_1;
            },
            function (word_direction_1_1) {
                word_direction_1 = word_direction_1_1;
            },
            function (player_turn_1_1) {
                player_turn_1 = player_turn_1_1;
            },
            function (exchange_tiles_1_1) {
                exchange_tiles_1 = exchange_tiles_1_1;
            }
        ],
        execute: function () {
            GameState = (function () {
                function GameState(wordLookupService) {
                    this.wordLookupService = wordLookupService;
                    this.inProgress = false;
                    this.currentPlayerIndex = 0;
                    this.playerTurn = null;
                    this.availableLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
                    this.lastPlayedBlank = null;
                    this.exchangeTiles = new exchange_tiles_1.ExchangeTiles();
                    this.gameOver = false;
                    this.playerTurn = new player_turn_1.PlayerTurn(null, null, this.wordLookupService);
                }
                GameState.prototype.isInProgress = function () {
                    return this.inProgress;
                };
                GameState.prototype.setInProgress = function (newState) {
                    this.inProgress = newState;
                };
                GameState.prototype.getBoard = function () {
                    return this.board;
                };
                GameState.prototype.setBoard = function (newBoard) {
                    this.board = newBoard;
                };
                GameState.prototype.setTileBag = function (newTileBag) {
                    this.tileBag = newTileBag;
                };
                GameState.prototype.getTileBag = function () {
                    return this.tileBag;
                };
                GameState.prototype.setPlayers = function (newPlayers, pickFirstPlayer) {
                    if (pickFirstPlayer === void 0) { pickFirstPlayer = false; }
                    this.players = newPlayers;
                    if (pickFirstPlayer) {
                        this.currentPlayerIndex = Math.floor(Math.random() * this.players.length);
                    }
                };
                GameState.prototype.getPlayers = function () {
                    return this.players;
                };
                GameState.prototype.isCurrentPlayer = function (playerIndex) {
                    return playerIndex == this.currentPlayerIndex;
                };
                GameState.prototype.currentPlayer = function () {
                    return this.players[this.currentPlayerIndex];
                };
                GameState.prototype.pass = function () {
                    // @TODO: We should ask for confirmation before continuing.
                    // @TODO: We must record how many passes there have been (game ends if everyone passes twice in a row).
                    this.moveToNextPlayer(true);
                };
                GameState.prototype.moveToNextPlayer = function (wasAPass) {
                    if (wasAPass === void 0) { wasAPass = false; }
                    // If they passed, we need to record it against the player; if they didn't, we need to
                    // reset it.
                    this.currentPlayer().updatedPassesCount(wasAPass);
                    // Check to see if the game is over. There are two criteria: either someone has no tiles in their
                    // rack, or everybody has passed twice in their last two turns.
                    if (!this.isEndOfGame()) {
                        // Move on to the next player.
                        this.currentPlayerIndex++;
                        this.currentPlayerIndex = this.currentPlayerIndex % this.players.length;
                    }
                };
                GameState.prototype.exchange = function () {
                    this.exchangeTiles = new exchange_tiles_1.ExchangeTiles(true);
                };
                GameState.prototype.isFirstMove = function () {
                    return !this.board.hasTileAt(new cell_coordinates_1.CellCoordinates(this.board.getMiddle(), this.board.getMiddle()));
                };
                GameState.prototype.startWordPositionIsValid = function (boardRowCell) {
                    if (this.isFirstMove()) {
                        // This is the first turn. Check that the word could go through the centre square.
                        var cellCoordinates = boardRowCell.getCellCoordinates();
                        var row = cellCoordinates.getRow();
                        var col = cellCoordinates.getCol();
                        var middle = this.board.getMiddle();
                        return ((row == middle && (col >= (middle - 6) && col <= middle)) ||
                            (col == middle && (row >= (middle - 6) && row <= middle)));
                    }
                    else {
                        // This is not the first turn. They can start almost anywhere, even if it's on the edge of the
                        // board or on top of a tile, or both. We just have to check that there is an empty square
                        // available in either direction.
                        if (this.board.hasEmptyCellFrom(boardRowCell.getCellCoordinates())) {
                            return true;
                        }
                    }
                    return false;
                };
                GameState.prototype.getAllowedDirectionsFromCell = function (boardRowCell) {
                    var allowedDirections = [];
                    // For now, we'll only deal with the case where this is the first move.
                    if (this.isFirstMove()) {
                        var cellCoordinates = boardRowCell.getCellCoordinates();
                        if (cellCoordinates.getRow() == this.board.getMiddle()) {
                            allowedDirections.push(word_direction_1.WordDirection.ACROSS);
                        }
                        if (cellCoordinates.getCol() == this.board.getMiddle()) {
                            allowedDirections.push(word_direction_1.WordDirection.DOWN);
                        }
                    }
                    else {
                        // It would be very difficult to validate the start point, so we delay the test to
                        // when they try to confirm their word.
                        allowedDirections.push(word_direction_1.WordDirection.ACROSS);
                        allowedDirections.push(word_direction_1.WordDirection.DOWN);
                    }
                    return allowedDirections;
                };
                GameState.prototype.startPlayingTiles = function (startClickedCell, wordDirection) {
                    // If the clicked cell contains a tile, we need to "backtrack" to find what would
                    // be the first tile in the word, and pass this cell into the PlayerTurn constructor.
                    startClickedCell = this.board.findInitialTile(startClickedCell, wordDirection);
                    this.playerTurn = new player_turn_1.PlayerTurn(startClickedCell, wordDirection, this.wordLookupService);
                };
                GameState.prototype.canSelectATile = function () {
                    return this.playerTurn.canAddTiles() || this.exchangeTiles.canExchangeTiles();
                };
                GameState.prototype.playTile = function (tileRackTileIndex) {
                    // We need to add the tile to the player turn and to the board.
                    var tileRackTile = this.currentPlayer().getTileRack()[tileRackTileIndex];
                    var playedTile = tileRackTile.getTile();
                    // Attempt to play the tile. If we can't, do nothing.
                    if (this.board.playTile(this.playerTurn, playedTile)) {
                        this.playerTurn.addTile(playedTile, this.board); // This must come after playTile() (on the line before).
                        tileRackTile.setSelected();
                        return true;
                    }
                    return false;
                };
                GameState.prototype.removeLastTile = function () {
                    this.board.removeLastTile(this.playerTurn);
                    var removedTile = this.playerTurn.removeTile(this.board); // This must be called after removeLastTile() (in the line above).
                    this.currentPlayer().unplayTile(removedTile);
                };
                GameState.prototype.tilesHaveBeenPlayed = function () {
                    return this.playerTurn.tilesHaveBeenPlayed();
                };
                GameState.prototype.cancelPlayTiles = function () {
                    // Only do this is they are currently playing pieces.
                    if (this.playerTurn.isInProgress()) {
                        // Remove all the played tiles from the board (as if user was removing them).
                        while (this.playerTurn.tilesHaveBeenPlayed()) {
                            this.removeLastTile();
                        }
                        // And then "switch off" the player turn.
                        this.playerTurn = new player_turn_1.PlayerTurn(null, null, this.wordLookupService);
                    }
                };
                GameState.prototype.getPlayedWords = function () {
                    return this.playerTurn.getWordsFormed().getWords();
                };
                GameState.prototype.getBonuses = function () {
                    return this.playerTurn.getWordsFormed().getBonuses();
                };
                GameState.prototype.getTurnScore = function () {
                    return this.playerTurn.getWordsFormed().getScore();
                };
                GameState.prototype.confirmPlayTiles = function () {
                    // They are confirming their word.
                    // @TODO: If any of the words are not valid, challenge them on it.
                    // We need to do a few things:
                    //  Add the turn score onto the player score.
                    //  Set the tiles in stone on the board.
                    //  Reset the player turn.
                    //  Remove the played tiles from the rack and refill from the tile bag.
                    //  Move play on to the next player.
                    this.currentPlayer().addToScore(this.playerTurn.getWordsFormed().getScore());
                    this.board.wordIsConfirmed();
                    this.playerTurn = new player_turn_1.PlayerTurn(null, null, this.wordLookupService);
                    this.currentPlayer().wordIsConfirmed(this.tileBag);
                    this.moveToNextPlayer();
                };
                GameState.prototype.isTouchingExistingTile = function () {
                    return this.playerTurn.isTouchingExistingTile();
                };
                GameState.prototype.hasSingleLetterWord = function () {
                    return this.playerTurn.hasSingleLetterWord();
                };
                GameState.prototype.setLastPlayedBlank = function (tile) {
                    this.lastPlayedBlank = tile;
                };
                GameState.prototype.selectLetter = function (whichLetter) {
                    if (this.availableLetters.indexOf(whichLetter) == -1) {
                        return false;
                    }
                    this.lastPlayedBlank.setLetterForBlank(whichLetter);
                    this.playerTurn.getWordsFormed().update(this.board, this.playerTurn);
                    return true;
                };
                GameState.prototype.exchangeOneTile = function (tileRackTileIndex) {
                    // We need to add the tile to the list of exchanged tiles.
                    var tileRackTile = this.currentPlayer().getTileRack()[tileRackTileIndex];
                    var tileToExchange = tileRackTile.getTile();
                    // Attempt to exchange the tile. If we can't, do nothing.
                    if (this.exchangeTiles.exchangeFromRack(tileToExchange, this.tileBag)) {
                        tileRackTile.setSelected();
                    }
                };
                GameState.prototype.unexchangeTiles = function (exchangeTilesIndex) {
                    var removedTile = this.exchangeTiles.removeTile(exchangeTilesIndex);
                    this.currentPlayer().unplayTile(removedTile);
                };
                GameState.prototype.unexchangeAllTiles = function () {
                    while (this.exchangeTiles.hasTiles()) {
                        this.unexchangeTiles(0);
                    }
                    this.exchangeTiles = new exchange_tiles_1.ExchangeTiles();
                };
                GameState.prototype.enoughTilesInBag = function () {
                    return this.exchangeTiles.enoughTilesInBag(this.tileBag);
                };
                GameState.prototype.confirmExchange = function () {
                    // Extract the exchanged tiles from the rack and replenish the rack from from bag.
                    // Put the exchanged tiles back into the bag and shuffle the bag.
                    this.currentPlayer().exchangeSelectedTiles(this.tileBag);
                    // Cancel the shuffle.
                    this.exchangeTiles = new exchange_tiles_1.ExchangeTiles();
                    // Move on to the next player.
                    this.moveToNextPlayer();
                };
                GameState.prototype.isGameOver = function () {
                    return this.gameOver;
                };
                GameState.prototype.isEndOfGame = function () {
                    // If we already know the game is over, just return true.
                    if (this.isGameOver()) {
                        return true;
                    }
                    // Now, there are two possibilities for the end of the game:
                    // 1) a player has zero tiles in their rack.
                    // 2) all players have passed in both of their last two turns.
                    // Loop through the players and set the two flags as appropriate.
                    var everyoneHasPassed = true;
                    var self = this;
                    this.players.forEach(function (player) {
                        everyoneHasPassed = everyoneHasPassed && player.hasPassedTwice();
                        self.gameOver = self.gameOver || player.hasNoTilesInRack();
                    });
                    this.gameOver = this.gameOver || everyoneHasPassed;
                    // If the game is now over and wasn't before, do all the adjustments.
                    if (this.gameOver) {
                        var playerWithZeroTiles_1 = null;
                        var totalAdjustments_1 = 0;
                        this.players.forEach(function (player) {
                            totalAdjustments_1 += player.calculateAdjustment();
                            if (player.hasNoTilesInRack()) {
                                playerWithZeroTiles_1 = player;
                            }
                        });
                        if (playerWithZeroTiles_1 != null) {
                            playerWithZeroTiles_1.setScoreAdjustment(totalAdjustments_1);
                        }
                        // And finally, we need to decide who has won. Bear in mind it could be a draw.
                        var highestScore_1 = -10000;
                        this.players.forEach(function (player) {
                            highestScore_1 = Math.max(highestScore_1, player.getScore());
                        });
                        this.players.forEach(function (player) {
                            player.testForVictory(highestScore_1);
                        });
                    }
                    return this.gameOver;
                };
                GameState.prototype.reset = function () {
                    this.gameOver = false;
                    // @TODO: We may need to reset all the players' scores.
                };
                GameState.prototype.getWordLookupApiCitation = function () {
                    return this.wordLookupService.getLanguageApi().getCitation();
                };
                GameState.prototype.hasWordLookupApiCitation = function () {
                    return this.getWordLookupApiCitation() != null;
                };
                return GameState;
            }());
            exports_1("GameState", GameState);
        }
    };
});
//# sourceMappingURL=game-state.js.map