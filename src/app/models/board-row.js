System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var BoardRow;
    return {
        setters: [],
        execute: function () {
            BoardRow = (function () {
                function BoardRow() {
                    this.boardRowCells = [];
                }
                BoardRow.prototype.addCell = function (boardRowCell) {
                    this.boardRowCells.push(boardRowCell);
                };
                BoardRow.prototype.setPremiumCell = function (premiumType, colIndex, isCentre) {
                    this.boardRowCells[colIndex].setPremiumCell(premiumType, isCentre);
                };
                BoardRow.prototype.getRowCells = function () {
                    return this.boardRowCells;
                };
                BoardRow.prototype.getTileAt = function (cellCoordinates) {
                    return this.boardRowCells[cellCoordinates.getCol()].getTile();
                };
                BoardRow.prototype.getTileStateAt = function (cellCoordinates) {
                    return this.boardRowCells[cellCoordinates.getCol()].getTileState();
                };
                BoardRow.prototype.wordIsConfirmed = function () {
                    for (var colIndex = 0; colIndex < this.boardRowCells.length; colIndex++) {
                        this.boardRowCells[colIndex].wordIsConfirmed();
                    }
                };
                return BoardRow;
            }());
            exports_1("BoardRow", BoardRow);
        }
    };
});
//# sourceMappingURL=board-row.js.map