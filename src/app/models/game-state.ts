import {TileBag} from './tile-bag';
import {Board} from './board';
import {Player} from './player';
import {BoardRowCell} from "./board-row-cell";
import {CellCoordinates} from "./cell-coordinates";
import {WordDirection} from "../enums/word-direction";
import {PlayerTurn} from "./player-turn";
import {PlayedWord} from "./played-word";
import {Tile} from "./tile";
import {ExchangeTiles} from "./exchange-tiles";
import {WordLookupService} from "../services/word-lookup.service";
import {GameStoreService} from "../services/game-store.service";
import {BoardGenerator} from "../generators/board-generator";

export class GameState {
    inProgress: boolean = false;
    board: Board;
    public tileBag: TileBag;
    players: Player[];
    currentPlayerIndex: number = 0;
    public playerTurn: PlayerTurn = null;
    public availableLetters: string[] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    private lastPlayedBlank: Tile = null;
    public exchangeTiles: ExchangeTiles = new ExchangeTiles();
    public gameOver: boolean = false;
    private uniqueID: number;
    private name: string;
    private numMoves = 0;

    constructor(private wordLookupService: WordLookupService, private gameStoreService: GameStoreService) {
        this.playerTurn = new PlayerTurn(null, null, this.wordLookupService);
    }

    public isInProgress(): boolean {
        return this.inProgress;
    }

    public setInProgress(newState: boolean) {
        this.inProgress = newState;
    }

    public getBoard() {
        return this.board;
    }

    public setBoard(newBoard: Board) {
        this.board = newBoard;
    }

    public setTileBag(newTileBag: TileBag) {
        this.tileBag = newTileBag;
    }

    public getTileBag(): TileBag {
        return this.tileBag;
    }

    public setPlayers(newPlayers: Player[], pickFirstPlayer: boolean = false) {
        this.players = newPlayers;
        if (pickFirstPlayer) {
            this.currentPlayerIndex = Math.floor(Math.random() * this.players.length);
        }
    }

    public getPlayers(): Player[] {
        return this.players;
    }

    public isCurrentPlayer(playerIndex: number): boolean {
        return playerIndex == this.currentPlayerIndex;
    }

    public currentPlayer(): Player {
        return this.players[this.currentPlayerIndex];
    }

    public pass(): boolean {
        if (!confirm('Are you sure you want to pass?')) {

            return false;
        }
        this.moveToNextPlayer(true);

        return true;
    }

    public moveToNextPlayer(wasAPass: boolean = false): void {
        // If they passed, we need to record it against the player; if they didn't, we need to
        // reset it.
        this.currentPlayer().updatedPassesCount(wasAPass);
        // Check to see if the game is over. There are two criteria: either someone has no tiles in their
        // rack, or everybody has passed in both of their last two turns.
        if (!this.isEndOfGame()) {
            // Move on to the next player.
            this.currentPlayerIndex++;
            this.currentPlayerIndex = this.currentPlayerIndex % this.players.length;
        }
        this.gameStoreService.saveCurrentGameState(this, ++this.numMoves);
    }

    public exchange() {
        this.exchangeTiles = new ExchangeTiles(true);
    }

    public isFirstMove(): boolean {
        return !this.board.hasTileAt(new CellCoordinates(this.board.getMiddle(), this.board.getMiddle()));
    }

    public afterFirstMove(): boolean {
        return this.numMoves > 0;
    }

    public startWordPositionIsValid(boardRowCell: BoardRowCell): boolean {
        if (this.isFirstMove()) {
            // This is the first turn. Check that the word could go through the centre square.
            let cellCoordinates = boardRowCell.getCellCoordinates();
            let row = cellCoordinates.getRow();
            let col = cellCoordinates.getCol();
            let middle = this.board.getMiddle();

            return (
                (row == middle && (col >= (middle - 6) && col <= middle)) ||
                (col == middle && (row >= (middle - 6) && row <= middle))
            );
        } else {
            // This is not the first turn. They can start almost anywhere, even if it's on the edge of the
            // board or on top of a tile, or both. We just have to check that there is an empty square
            // available in either direction.
            if (this.board.hasEmptyCellFrom(boardRowCell.getCellCoordinates())) {

                return true;
            }

        }

        return false;
    }

    public getAllowedDirectionsFromCell(boardRowCell): WordDirection[] {
        let allowedDirections = [];
        let cellCoordinates = boardRowCell.getCellCoordinates();
        // The logic depends on whether or not this is the first move.
        if (this.isFirstMove()) {
            if (cellCoordinates.getRow() == this.board.getMiddle()) {
                allowedDirections.push(WordDirection.ACROSS);
            }
            if (cellCoordinates.getCol() == this.board.getMiddle()) {
                allowedDirections.push(WordDirection.DOWN);
            }
        } else {
            // Move up to 7 empty squares across and down, stopping if we hit the edge of the board,
            // and see which direction results in an empty square that touches an adjacent,
            // populated square.
            let testCoordinate = [];
            testCoordinate[0] = new CellCoordinates(cellCoordinates.getRow(), cellCoordinates.getCol());
            testCoordinate[1] = new CellCoordinates(cellCoordinates.getRow(), cellCoordinates.getCol());
            let tmpAllowedDirs = [];
            tmpAllowedDirs[0] = false;
            tmpAllowedDirs[1] = false;
            for (let count = 0 ; count < 7 ; count++) {
                for (let dir = 0 ; dir < 2 ; dir++) {
                    let currentCoordinate = testCoordinate[dir];
                    if (this.board.positionIsValid(currentCoordinate) &&
                        !this.board.hasTileAt(currentCoordinate)) {
                        // We are looking at a valid cell on the board that has no tile on it.
                        // Look at the 4 spaces above, below, left and right. If any have a tile
                        // on, then this direction is allowed.
                        let cellUp = new CellCoordinates(currentCoordinate.getRow()-1, currentCoordinate.getCol());
                        let cellDown = new CellCoordinates(currentCoordinate.getRow()+1, currentCoordinate.getCol());
                        let cellLeft = new CellCoordinates(currentCoordinate.getRow(), currentCoordinate.getCol()-1);
                        let cellRight = new CellCoordinates(currentCoordinate.getRow(), currentCoordinate.getCol()+1);
                        if ((this.board.positionIsValid(cellUp) && this.board.hasTileAt(cellUp)) ||
                            (this.board.positionIsValid(cellDown) && this.board.hasTileAt(cellDown)) ||
                            (this.board.positionIsValid(cellLeft) && this.board.hasTileAt(cellLeft)) ||
                            (this.board.positionIsValid(cellRight) && this.board.hasTileAt(cellRight))) {
                            // Yes there is an adjacent cell with a tile on it - this direction is valid!
                            tmpAllowedDirs[dir] = true;
                        }
                    }
                }
                testCoordinate[0].incrementCol();
                testCoordinate[1].incrementRow();
            }
            // Push the appropriate directions, if any into the return value.
            if (tmpAllowedDirs[0]) {
                allowedDirections.push(WordDirection.ACROSS);
            }
            if (tmpAllowedDirs[1]) {
                allowedDirections.push(WordDirection.DOWN);
            }
        }

        return allowedDirections;
    }

    public startPlayingTiles(startClickedCell: BoardRowCell, wordDirection: WordDirection): void {
        // If the clicked cell contains a tile, we need to "backtrack" to find what would
        // be the first tile in the word, and pass this cell into the PlayerTurn constructor.
        startClickedCell = this.board.findInitialTile(startClickedCell, wordDirection);
        this.playerTurn = new PlayerTurn(startClickedCell, wordDirection, this.wordLookupService);
    }

    public canSelectATile(): boolean {
        return this.playerTurn.canAddTiles() || this.exchangeTiles.canExchangeTiles();
    }

    public playTile(tileRackTileIndex: number): boolean {
        // We need to add the tile to the player turn and to the board.
        let tileRackTile = this.currentPlayer().getTileRack()[tileRackTileIndex];
        let playedTile = tileRackTile.getTile();
        // Attempt to play the tile. If we can't, do nothing.
        if (this.board.playTile(this.playerTurn, playedTile)) {
            this.playerTurn.addTile(playedTile, this.board); // This must come after playTile() (on the line before).
            tileRackTile.setSelected();

            return true;
        }

        return false;
    }

    public removeLastTile(): void {
        this.board.removeLastTile(this.playerTurn);
        let removedTile = this.playerTurn.removeTile(this.board); // This must be called after removeLastTile() (in the line above).
        this.currentPlayer().unplayTile(removedTile);
    }

    public tilesHaveBeenPlayed(): boolean {
        return this.playerTurn.tilesHaveBeenPlayed();
    }

    public cancelPlayTiles(): void {
        // Only do this is they are currently playing pieces.
        if (this.playerTurn.isInProgress()) {
            // Remove all the played tiles from the board (as if user was removing them).
            while (this.playerTurn.tilesHaveBeenPlayed()) {
                this.removeLastTile();
            }
            // And then "switch off" the player turn.
            this.playerTurn = new PlayerTurn(null, null, this.wordLookupService);
        }
    }

    public getPlayedWords(): PlayedWord[] {
        return this.playerTurn.getWordsFormed().getWords();
    }

    public getBonuses(): PlayedWord[] {
        return this.playerTurn.getWordsFormed().getBonuses();
    }

    public getTurnScore(): number {
        return this.playerTurn.getWordsFormed().getScore();
    }

    public confirmPlayTiles(): void {
        // They are confirming their word.
        // @TODO: If any of the words are not valid, challenge them on it.
        // We need to do a few things:
        //  Add the turn score onto the player score.
        //  Set the tiles in stone on the board.
        //  Reset the player turn.
        //  Remove the played tiles from the rack and refill from the tile bag.
        //  Move play on to the next player.
        this.currentPlayer().addToScore(this.playerTurn.getWordsFormed().getScore());
        this.board.wordIsConfirmed();
        this.playerTurn = new PlayerTurn(null, null, this.wordLookupService);
        this.currentPlayer().wordIsConfirmed(this.tileBag);
        this.moveToNextPlayer();
    }

    public isTouchingExistingTile(): boolean {
        return this.playerTurn.isTouchingExistingTile();
    }

    public hasSingleLetterWord(): boolean {
        return this.playerTurn.hasSingleLetterWord();
    }

    public setLastPlayedBlank(tile: Tile): void {
        this.lastPlayedBlank = tile;
    }

    public selectLetter(whichLetter: string): boolean {
        if (this.availableLetters.indexOf(whichLetter) == -1) {

            return false;
        }
        this.lastPlayedBlank.setLetterForBlank(whichLetter);
        this.playerTurn.getWordsFormed().update(this.board, this.playerTurn);

        return true;
    }

    public exchangeOneTile(tileRackTileIndex: number): void {
        // We need to add the tile to the list of exchanged tiles.
        let tileRackTile = this.currentPlayer().getTileRack()[tileRackTileIndex];
        let tileToExchange = tileRackTile.getTile();
        // Attempt to exchange the tile. If we can't, do nothing.
        if (this.exchangeTiles.exchangeFromRack(tileToExchange, this.tileBag)) {
            tileRackTile.setSelected();
        }
    }

    public unexchangeTiles(exchangeTilesIndex) {
        let removedTile = this.exchangeTiles.removeTile(exchangeTilesIndex);
        this.currentPlayer().unplayTile(removedTile);
    }

    public unexchangeAllTiles(): void {

        while (this.exchangeTiles.hasTiles()) {
            this.unexchangeTiles(0);
        }
        this.exchangeTiles = new ExchangeTiles();
    }

    public enoughTilesInBag(): boolean {
        return this.exchangeTiles.enoughTilesInBag(this.tileBag);
    }

    public confirmExchange() {
        // Extract the exchanged tiles from the rack and replenish the rack from from bag.
        // Put the exchanged tiles back into the bag and shuffle the bag.
        this.currentPlayer().exchangeSelectedTiles(this.tileBag);

        // Cancel the shuffle.
        this.exchangeTiles = new ExchangeTiles();

        // Move on to the next player.
        this.moveToNextPlayer();
    }

    public isGameOver() {
        return this.gameOver;
    }

    public isEndOfGame(): boolean {
        // If we already know the game is over, just return true.
        if (this.isGameOver()) {

            return true;
        }

        // Now, there are two possibilities for the end of the game:
        // 1) a player has zero tiles in their rack.
        // 2) all players have passed in both of their last two turns.

        // Loop through the players and set the two flags as appropriate.
        let everyoneHasPassed = true;
        let self = this;
        this.players.forEach(function(player: Player) {
            everyoneHasPassed = everyoneHasPassed && player.hasPassedTwice();
            self.gameOver = self.gameOver || player.hasNoTilesInRack();
        });
        this.gameOver = this.gameOver || everyoneHasPassed;

        // If the game is now over and wasn't before, do all the adjustments.
        if (this.gameOver) {
            let playerWithZeroTiles: Player = null;
            let totalAdjustments = 0;
            this.players.forEach(function(player: Player) {
                totalAdjustments += player.calculateAdjustment();
                if (player.hasNoTilesInRack()) {
                    playerWithZeroTiles = player;
                }
            });
            if (playerWithZeroTiles != null) {
                playerWithZeroTiles.setScoreAdjustment(totalAdjustments);
            }

            // And finally, we need to decide who has won. Bear in mind it could be a draw.
            let highestScore = -10000;
            this.players.forEach(function(player: Player) {
                highestScore = Math.max(highestScore, player.getScore());
            });
            this.players.forEach(function(player: Player) {
                player.testForVictory(highestScore);
            });

        }

        return this.gameOver;
    }

    public reset(): void {
        this.gameOver = false;
        // @TODO: We may need to reset all the players' scores.
    }

    public getWordLookupApiCitation(): string {
        return this.wordLookupService.getLanguageDriver().getCitation();
    }

    public hasWordLookupApiCitation(): boolean {
        return this.getWordLookupApiCitation() != null;
    }

    public generateUniqueID() {
        // For now, just use the time.
        this.uniqueID = (new Date()).getTime();
    }

    public getUniqueID() {
        return this.uniqueID;
    }

    public setUniqueID(uniqueID: number) {
        if (!this.uniqueID) {
            this.uniqueID = uniqueID
        }
    }

    public getName(): string {
        return this.name;
    }
    public setName(newName: string): void {
        this.name = newName;
    }

    public decrementMoveNumber(): number {
        this.numMoves--;

        return this.numMoves;
    }

    public incrementMoveNumber(): number {
        this.numMoves++;

        return this.numMoves;
    }

    public restoreState(newPlayerIndex: number, gameIsFinished: boolean) {
        this.currentPlayerIndex = newPlayerIndex;
        this.gameOver = gameIsFinished;
    }

    public getNumMoves(): number {
        return this.numMoves;
    }

    public restoreBoard(savedBoard: any): void {
        // We need to emulate a board config based on the saved board.
        let ourConfig = {
            size: savedBoard.size,
            premiumCells: []
        };
        for(let premIdx = 0 ; premIdx < savedBoard.premiumCells.length ; premIdx++) {
            let premiumCell = savedBoard.premiumCells[premIdx];
            ourConfig.premiumCells.push({
                cellType: premiumCell.type,
                cells: [[premiumCell.row, premiumCell.col]] // Yes, I know it looks a bit weird.
            });
        }
        this.setBoard((new BoardGenerator()).generate(ourConfig));
    }

    public restorePlayers(savedPlayers: any): void {
        this.players = [];
        for (let playerIdx = 0 ; playerIdx < savedPlayers.length ; playerIdx++) {
            this.players.push(new Player(savedPlayers[playerIdx].name, savedPlayers[playerIdx].isHuman, null));
        }
    }

    public restoreTileBag(savedTileBag: any): void {
        this.tileBag = new TileBag();
        for (let tileIdx = 0 ; tileIdx < savedTileBag.length ; tileIdx++) {
            this.tileBag.add(new Tile(savedTileBag[tileIdx].letter, savedTileBag[tileIdx].pointsValue));
        }
    }
}
