System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var CellCoordinates;
    return {
        setters: [],
        execute: function () {
            // Represents a row/column pair.
            CellCoordinates = (function () {
                function CellCoordinates(row, col) {
                    this.row = row;
                    this.col = col;
                }
                ;
                CellCoordinates.prototype.getRow = function () {
                    return this.row;
                };
                CellCoordinates.prototype.getCol = function () {
                    return this.col;
                };
                CellCoordinates.prototype.incrementCol = function () {
                    this.col++;
                };
                CellCoordinates.prototype.incrementRow = function () {
                    this.row++;
                };
                return CellCoordinates;
            }());
            exports_1("CellCoordinates", CellCoordinates);
        }
    };
});
//# sourceMappingURL=cell-coordinates.js.map