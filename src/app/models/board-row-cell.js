System.register(["../enums/board-tile-state", "../enums/premium-cell-type"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var board_tile_state_1, premium_cell_type_1, BoardRowCell;
    return {
        setters: [
            function (board_tile_state_1_1) {
                board_tile_state_1 = board_tile_state_1_1;
            },
            function (premium_cell_type_1_1) {
                premium_cell_type_1 = premium_cell_type_1_1;
            }
        ],
        execute: function () {
            BoardRowCell = (function () {
                function BoardRowCell(cellCoordinates) {
                    this.cellCoordinates = cellCoordinates;
                    this.cellType = null;
                    this.placedTile = null;
                    this.tileState = null;
                    this.className = 'none'; // The default cell colour.
                    this.cellTooltip = null;
                    this.cellText = null;
                    this.cellCoordinates = cellCoordinates;
                }
                BoardRowCell.prototype.setPremiumCell = function (premiumCellType, isCentre) {
                    this.cellType = premiumCellType;
                    // Set other attributes now that we know the cell type.
                    switch (this.cellType) {
                        case premium_cell_type_1.PremiumCellType.DLS:
                            this.className = 'dls';
                            this.cellTooltip = 'Double Letter Score';
                            this.cellText = 'DL';
                            break;
                        case premium_cell_type_1.PremiumCellType.TLS:
                            this.className = 'tls';
                            this.cellTooltip = 'Triple Letter Score';
                            this.cellText = 'TL';
                            break;
                        case premium_cell_type_1.PremiumCellType.QLS:
                            this.className = 'qls';
                            this.cellTooltip = 'Quadruple Letter Score';
                            this.cellText = 'QL';
                            break;
                        case premium_cell_type_1.PremiumCellType.DWS:
                            this.className = 'dws';
                            this.cellTooltip = 'Double Word Score';
                            this.cellText = 'DW';
                            break;
                        case premium_cell_type_1.PremiumCellType.TWS:
                            this.className = 'tws';
                            this.cellTooltip = 'Triple Word Score';
                            this.cellText = 'TW';
                            break;
                        case premium_cell_type_1.PremiumCellType.QWS:
                            this.className = 'qws';
                            this.cellTooltip = 'Quadruple Word Score';
                            this.cellText = 'QW';
                            break;
                    }
                    if (isCentre && this.cellText != null) {
                        this.cellText = '*';
                    }
                };
                BoardRowCell.prototype.getPremiumType = function () {
                    return this.cellType;
                };
                BoardRowCell.prototype.getClassName = function () {
                    return this.className;
                };
                BoardRowCell.prototype.getCellTooltip = function () {
                    return this.cellTooltip;
                };
                BoardRowCell.prototype.getCellText = function () {
                    return this.cellText;
                };
                BoardRowCell.prototype.getCellCoordinates = function () {
                    return this.cellCoordinates;
                };
                BoardRowCell.prototype.getTile = function () {
                    return this.placedTile;
                };
                BoardRowCell.prototype.getTileState = function () {
                    return this.tileState;
                };
                BoardRowCell.prototype.setTile = function (playedTile) {
                    this.placedTile = playedTile;
                    this.tileState = board_tile_state_1.BoardTileState.IN_TURN;
                };
                BoardRowCell.prototype.hasTile = function () {
                    return !(this.placedTile == null);
                };
                BoardRowCell.prototype.clearTile = function () {
                    this.placedTile.unsetLetterForBlank();
                    this.placedTile = null;
                    this.tileState = null;
                };
                BoardRowCell.prototype.wordIsConfirmed = function () {
                    // Transform the states as follows:
                    // JUST_PLAYED => CONFIRMED
                    // IN_TURN => JUST_PLAYED
                    switch (this.tileState) {
                        case board_tile_state_1.BoardTileState.JUST_PLAYED:
                            this.tileState = board_tile_state_1.BoardTileState.CONFIRMED;
                            break;
                        case board_tile_state_1.BoardTileState.IN_TURN:
                            this.tileState = board_tile_state_1.BoardTileState.JUST_PLAYED;
                            break;
                        default:
                            break;
                    }
                };
                BoardRowCell.prototype.getCssSubclass = function () {
                    var cssSubClass = '';
                    switch (this.tileState) {
                        case board_tile_state_1.BoardTileState.IN_TURN:
                            cssSubClass = 'in-turn';
                            break;
                        case board_tile_state_1.BoardTileState.JUST_PLAYED:
                            cssSubClass = 'just-played';
                            break;
                        case board_tile_state_1.BoardTileState.CONFIRMED:
                            cssSubClass = 'confirmed';
                            break;
                        default:
                            break;
                    }
                    return cssSubClass;
                };
                return BoardRowCell;
            }());
            exports_1("BoardRowCell", BoardRowCell);
        }
    };
});
//# sourceMappingURL=board-row-cell.js.map