// This object manages the user exchanging one or more tiles as part of their go.
import {Tile} from "./tile";
import {TileBag} from "./tile-bag";

export class ExchangeTiles {
    private inProgress: boolean = false; // Is someone currently exchanging their tiles?
    private tilesToExchange: Tile[] = [];
    constructor(started?: boolean) {
        this.inProgress = !!started;
        if (this.inProgress) {
            this.tilesToExchange = [];
        }
    }

    public isInProgress(): boolean {
        return this.inProgress;
    }

    public canExchangeTiles(): boolean {
        return this.inProgress;
    }

    public getTiles(): Tile[] {
        return this.tilesToExchange;
    }

    public exchangeFromRack(tileToExchange: Tile, tileBag: TileBag): boolean {
        // Make sure there are enough tiles left in the bag.
        if (!this.enoughTilesInBag(tileBag)) {

            return false;
        }
        this.tilesToExchange.push(tileToExchange);

        return true;
    }

    public removeTile(exchangeTilesIndex): Tile {
        return this.tilesToExchange.splice(exchangeTilesIndex, 1).shift();
    }

    public hasTiles(): boolean {
        return this.tilesToExchange.length > 0;
    }

    public enoughTilesInBag(tileBag: TileBag) {
        return (tileBag.howManyTiles() > this.tilesToExchange.length);
    }
}