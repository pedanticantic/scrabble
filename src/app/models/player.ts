import {Tile} from "./tile";
import {Shuffler} from '../services/shuffler';
import {TileRackTile} from "./tile-rack-tile";
import {TileBag} from "./tile-bag";

export class Player {
    name: string;
    isHuman: boolean;
    computerLevel: number;
    tileRack: TileRackTile[];
    score: number = 0;
    adjustmentAmount: number = 0;
    shuffler: Shuffler = new Shuffler();
    numConsecutivePasses: number = 0;
    hasWon: boolean = false;
    constructor(newName: string, newIsHuman: boolean, newComputerLevel: number) {
        this.name = newName;
        this.isHuman = newIsHuman;
        this.computerLevel = newComputerLevel;
        this.tileRack = [];
    }

    public getName(): string {
        return this.name;
    }

    public isRackFull() {
        return this.tileRack.length >= 7;
    }

    public addTileToRack(tile: Tile) {
        if (this.isRackFull()) {
            console.log('Something it trying to overfill a rack!');
        }
        this.tileRack.push(new TileRackTile(tile));
    }

    public getTileRack(): TileRackTile[] {
        return this.tileRack;
    }

    public getScore(): number {
        return this.score;
    }

    public getScoreAdjustment(): number {
        return this.adjustmentAmount;
    }

    public shuffleRack() {
        this.shuffler.shuffle(this.tileRack);
    }

    public hasTileBeenSelected(tileRackTileIndex): boolean {
        // @TODO: We should to a range check on the passed in index.
        return this.tileRack[tileRackTileIndex].getHasBeenSelected();
    }

    public unplayTile(removedTile: Tile): void {
        // We loop through the rack from right to left so that it unselects in a natural order.
        for(let tileIndex = this.tileRack.length - 1 ; tileIndex >= 0  ; tileIndex--) {
            if (removedTile.getLetter() == this.tileRack[tileIndex].getTile().getLetter()) {
                if (this.tileRack[tileIndex].getHasBeenSelected()) {
                    this.tileRack[tileIndex].unsetSelected();
                    break;
                }
            }
        }
    }

    public wordIsConfirmed(tileBag: TileBag): void {
        // We need to remove any tiles played in the turn from the rack, and
        // replenish them from the bag.
        this.tileRack = this.tileRack.filter(function(tileRackTile: TileRackTile) {
            return !tileRackTile.getHasBeenSelected();
        });
        this.replenishFromTileBag(tileBag);
    }

    public replenishFromTileBag(tileBag: TileBag) {
        while (!this.isRackFull() && !tileBag.isEmpty()) {
            this.addTileToRack(tileBag.takeTile());
        }
    }

    /**
     * Method to add the given number onto the player's score. The number can be
     * negative (as at the end of the game).
     * @param delta number
     */
    public addToScore(delta: number): void {
        this.score += delta;
    }

    public exchangeSelectedTiles(tileBag: TileBag): void {
        // Okay, split the rack into an array of tiles that were selected, and the remaining
        // tile rack tile objects.
        let extractedTiles: Tile[] = [];
        let newTileRack: TileRackTile[] = [];
        this.tileRack.forEach(function(tileRackTile: TileRackTile) {
            if (tileRackTile.getHasBeenSelected()) {
                extractedTiles.push(tileRackTile.getTile());
            } else {
                newTileRack.push(tileRackTile);
            }
        });
        this.tileRack = newTileRack;

        // Now replenish the rack from the bag. It's important to do this before putting the
        // selected tiles back into the bag, or they might get one of the same tiles back again.
        this.replenishFromTileBag(tileBag);

        // Now, put the selected tiles back into the bag. The bag knows to shuffle the tiles after.
        tileBag.replaceTiles(extractedTiles);
    }

    public updatedPassesCount(wasAPass: boolean) {
        if (wasAPass) {
            this.numConsecutivePasses++;
        } else {
            this.numConsecutivePasses = 0;
        }
    }

    public hasPassedTwice(): boolean {
        return this.numConsecutivePasses >= 2;
    }

    public hasNoTilesInRack(): boolean {
        return this.tileRack.length == 0;
    }

    public calculateAdjustment(): number {
        // We simply add up the scores on all the tiles in the rack, then subtract them
        // from the player score, set the adjustment amount, and return the amount.
        let adjustment = 0;
        this.tileRack.forEach(function(tileRackTile: TileRackTile) {
            adjustment += tileRackTile.getTile().getPointsValue();
        });
        this.setScoreAdjustment(-1 * adjustment);

        return adjustment;
    }

    public setScoreAdjustment(adjustmentAmount: number) {
        this.score += adjustmentAmount;
        this.adjustmentAmount = adjustmentAmount;
    }

    public testForVictory(highestScore: number): void {
        this.hasWon = (this.score == highestScore);
    }

    public isWinner(): boolean {
        return this.hasWon;
    }

    public initialise(tileBag: TileBag): void {
        this.tileRack = [];
        this.replenishFromTileBag(tileBag);
        this.score = 0;
        this.adjustmentAmount = 0;
        this.numConsecutivePasses = 0;
        this.hasWon = false;
    }

    public restoreState(previousState: any) {
        this.score = previousState.score;
        this.adjustmentAmount = previousState.scoreAdjustment;
        this.numConsecutivePasses = previousState.passes;
        this.tileRack = [];
        let self = this;
        previousState.tileRack.forEach(function (tile: any) {
            self.addTileToRack(new Tile(tile.letter, tile.pointsValue));
        })
    }
}
