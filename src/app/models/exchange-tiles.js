System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ExchangeTiles;
    return {
        setters: [],
        execute: function () {
            ExchangeTiles = (function () {
                function ExchangeTiles(started) {
                    this.inProgress = false; // Is someone currently exchanging their tiles?
                    this.tilesToExchange = [];
                    this.inProgress = !!started;
                    if (this.inProgress) {
                        this.tilesToExchange = [];
                    }
                }
                ExchangeTiles.prototype.isInProgress = function () {
                    return this.inProgress;
                };
                ExchangeTiles.prototype.canExchangeTiles = function () {
                    return this.inProgress;
                };
                ExchangeTiles.prototype.getTiles = function () {
                    return this.tilesToExchange;
                };
                ExchangeTiles.prototype.exchangeFromRack = function (tileToExchange, tileBag) {
                    // Make sure there are enough tiles left in the bag.
                    if (!this.enoughTilesInBag(tileBag)) {
                        return false;
                    }
                    this.tilesToExchange.push(tileToExchange);
                    return true;
                };
                ExchangeTiles.prototype.removeTile = function (exchangeTilesIndex) {
                    return this.tilesToExchange.splice(exchangeTilesIndex, 1).shift();
                };
                ExchangeTiles.prototype.hasTiles = function () {
                    return this.tilesToExchange.length > 0;
                };
                ExchangeTiles.prototype.enoughTilesInBag = function (tileBag) {
                    return (tileBag.howManyTiles() > this.tilesToExchange.length);
                };
                return ExchangeTiles;
            }());
            exports_1("ExchangeTiles", ExchangeTiles);
        }
    };
});
//# sourceMappingURL=exchange-tiles.js.map