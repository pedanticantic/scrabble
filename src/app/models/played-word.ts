// Represents an instance of a word that is being played in this turn.
// It is also used to hold any bonuses.
import {ValidationState} from "../enums/validation-state";
import {WordLookupResponse} from "../value-objects/WordLookupResponse";
export class PlayedWord {
    private validationResponse: WordLookupResponse = new WordLookupResponse(ValidationState.IN_PROGRESS);
    private isVisible: boolean;

    constructor(private word: string, private score: number) {};

    public getWord(): string {
        return this.word;
    }

    public getScore(): number {
        return this.score;
    }

    public getWordLength(): number {
        return this.word.length;
    }

    public asLowerCase(): string {
        return this.word.toLowerCase();
    }

    public getValidationState(): string {
        let result = 'UNKNOWN';
        switch(this.validationResponse.getValidState()) {
            case ValidationState.VALID:
                result = 'VALID';
                break;
            case ValidationState.NOT_VALID:
                result = 'NOT VALID';
                break;
            case ValidationState.ERROR:
                result = 'ERROR';
                break;
        }

        return result;
    }

    public getDefinitions(): string[] {
        return this.validationResponse.getDefinitions();
    }

    public setValidationResponse(response: WordLookupResponse): void {
        this.validationResponse = response;
    }

    public showHideDefn(newState: boolean): void {
        this.isVisible = newState;
    }

    public definitionsAreShown(): boolean {
        return this.isVisible;
    }
}
