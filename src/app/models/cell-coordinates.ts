// Represents a row/column pair.
export class CellCoordinates {
    constructor(private row: number, private col: number) {};

    public getRow(): number {
        return this.row;
    }

    public getCol(): number {
        return this.col;
    }

    public incrementCol(): void {
        this.col++;
    }

    public incrementRow(): void {
        this.row++;
    }

}