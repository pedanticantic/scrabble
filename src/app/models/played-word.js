System.register(["../enums/validation-state"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var validation_state_1, PlayedWord;
    return {
        setters: [
            function (validation_state_1_1) {
                validation_state_1 = validation_state_1_1;
            }
        ],
        execute: function () {
            PlayedWord = (function () {
                function PlayedWord(word, score) {
                    this.word = word;
                    this.score = score;
                    this.validationState = validation_state_1.ValidationState.IN_PROGRESS;
                }
                ;
                PlayedWord.prototype.getWord = function () {
                    return this.word;
                };
                PlayedWord.prototype.getScore = function () {
                    return this.score;
                };
                PlayedWord.prototype.getWordLength = function () {
                    return this.word.length;
                };
                PlayedWord.prototype.asLowerCase = function () {
                    return this.word.toLowerCase();
                };
                PlayedWord.prototype.getValidationState = function () {
                    var result = 'UNKNOWN';
                    switch (this.validationState) {
                        case validation_state_1.ValidationState.VALID:
                            result = 'VALID';
                            break;
                        case validation_state_1.ValidationState.NOT_VALID:
                            result = 'NOT VALID';
                            break;
                    }
                    return result;
                };
                PlayedWord.prototype.setValidationResponse = function (newState) {
                    this.validationState = newState;
                };
                return PlayedWord;
            }());
            exports_1("PlayedWord", PlayedWord);
        }
    };
});
//# sourceMappingURL=played-word.js.map