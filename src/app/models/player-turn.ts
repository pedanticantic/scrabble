// This object holds data relating to a player's turn. It's used in conjunction with
// the board and current player to manage that player's turn.
import {CellCoordinates} from "./cell-coordinates";
import {WordDirection} from "../enums/word-direction";
import {BoardRowCell} from "./board-row-cell";
import {Tile} from "./tile";
import {WordsFormed} from "./words-formed";
import {Board} from "./board";
import {BoardTileState} from "../enums/board-tile-state";
import {WordLookupService} from "../services/word-lookup.service";

export class PlayerTurn {
    private inProgress: boolean = false; // Is someone playing piece?
    private isAtEdgeOfBoard: boolean = false; // Have we hit the edge of the board?
    private firstTileCoordinates: CellCoordinates = null;
    private wordDirection: WordDirection = null;
    private playedTiles: Tile[] = []; // An array of the tiles placed.
    private wordsFormed: WordsFormed;
    private touchesAnotherWord: boolean = false;

    constructor(startClickedCell: BoardRowCell, wordDirection: WordDirection, wordLookupService: WordLookupService) {
        if (startClickedCell == null || wordDirection == null) {
            this.inProgress = false;
            this.firstTileCoordinates = null;
            this.wordDirection = null;
        } else {
            this.inProgress = true;
            this.firstTileCoordinates = startClickedCell.getCellCoordinates();
            this.wordDirection = wordDirection;
        }
        this.wordsFormed = new WordsFormed(wordLookupService);
    }

    public getWordDirection(): WordDirection {
        return this.wordDirection;
    }

    public isInProgress(): boolean {
        return this.inProgress;
    }

    public canAddTiles(): boolean {
        return this.inProgress && !this.isAtEdgeOfBoard;
    }

    public tilesHaveBeenPlayed(): boolean {
        return this.playedTiles.length > 0;
    }

    public addTile(nextTile: Tile, board: Board) {
        this.playedTiles.push(nextTile);
        // @TODO: Check whether we've now reached the edge of the board (may have to pass through previously played tiles on the board).

        // Update the list of words formed in this go.
        this.wordsFormed.update(board, this);

        // Determine whether this word touches other ones.
        this.saveTouchesOtherWords(board);
    }

    public removeTile(board: Board): Tile {
        if (this.playedTiles.length > 0) {
            let removedTile = this.playedTiles.pop();

            // Update the list of words formed in this go.
            this.wordsFormed.update(board, this);

            // Determine whether this word still touches other ones.
            this.saveTouchesOtherWords(board);

            return removedTile;
        }

        return null;
    }

    public getInitialPosition(): CellCoordinates {
        return this.firstTileCoordinates;
    }

    public hasPlayedAllTiles(): boolean {
        return (this.playedTiles.length == 7);
    }

    public getWordsFormed(): WordsFormed {
        return this.wordsFormed;
    }

    public saveTouchesOtherWords(board: Board): void {
        this.touchesAnotherWord = this.calcTouchesOtherWords(board);
    }

    private calcTouchesOtherWords(board: Board): boolean {
        // If this is the very first go, just return true.
        // @TODO: Don't hard-code the dimensions.
        let centreTileState = board.getTileStateAt(new CellCoordinates(7, 7));
        if (centreTileState == null || centreTileState == BoardTileState.IN_TURN) {

            return true;
        }

        // Special check: if the first letter of this word was not played in this go, then we must
        // be touching another word!
        let previousCellPosition = new CellCoordinates(
            this.firstTileCoordinates.getRow() - (this.wordDirection == WordDirection.ACROSS ? 0 : 1),
            this.firstTileCoordinates.getCol() - (this.wordDirection == WordDirection.ACROSS ? 1 : 0)
        );
        if (board.hasTileAt(previousCellPosition) && board.getTileStateAt(previousCellPosition) != BoardTileState.IN_TURN) {

            return true;
        }

        // Start at the first letter (even if played in previous turns).
        // Step through to the end of the word. If the tile was played in a previous go or
        // either adjacent square is populated, return true. If we get to the end of the word
        // without finding a touching tile, return false.
        let currentCoordinates = this.firstTileCoordinates;
        while(board.hasTileAt(currentCoordinates)) {
            // If the tile we're on now was laid in a previous turn, we are touching another word.
            if (board.getTileStateAt(currentCoordinates) != BoardTileState.IN_TURN) {

                return true;
            }

            let adjoiningDeltaRow = (this.wordDirection == WordDirection.ACROSS ? 1 : 0);
            let adjoiningDeltaCol = (this.wordDirection == WordDirection.ACROSS ? 0 : 1);
            var firstSide = new CellCoordinates(
                currentCoordinates.getRow() + adjoiningDeltaRow,
                currentCoordinates.getCol() + adjoiningDeltaCol
            );
            var secondSide = new CellCoordinates(
                currentCoordinates.getRow() - adjoiningDeltaRow,
                currentCoordinates.getCol() - adjoiningDeltaCol
            );
            if (board.hasTileAt(firstSide) || board.hasTileAt(secondSide)) {

                // This tile is adjoining another.
                return true;
            }

            currentCoordinates = new CellCoordinates(
                currentCoordinates.getRow() + (this.wordDirection == WordDirection.DOWN ? 1 : 0),
                currentCoordinates.getCol() + (this.wordDirection == WordDirection.ACROSS ? 1 : 0)
            );
        }

        // Did not find any adjoining tiles.
        return false;
    }

    public isTouchingExistingTile(): boolean {
        return this.touchesAnotherWord;
    }

    public hasSingleLetterWord(): boolean {
        return this.wordsFormed.hasSingleLetterWord();
    }
}