// Represents a single tile. The "letterIfBlank" gets populated when a player nominates
// a letter if the tile is blank.
export class Tile {
    tileLetter: string;
    pointsValue: number;
    letterIfBlank: string;

    constructor(tileLetter: string, pointsValue: number) {
        this.tileLetter = tileLetter;
        this.pointsValue = pointsValue;
        this.letterIfBlank = null;
    };

    public getLetter(): string {
        return this.tileLetter;
    }

    public getPointsValue(): number {
        return this.pointsValue;
    }

    public getPlacedLetter(useNBSp: boolean = false, showLetterIfBlank: string = '0'): string {
        return this.isBlank() ? (this.letterIfBlank == null ? (useNBSp ? '&nbsp;' : '?') : (showLetterIfBlank == '1' ? this.letterIfBlank : '&nbsp;')) : this.tileLetter;
    }

    public isBlank(): boolean {
        return this.tileLetter == ' ';
    }

    public setLetterForBlank(whichLetter: string): void {
        if (this.isBlank()) {
            this.letterIfBlank = whichLetter;
        }
    }

    public getLetterForBlank(): string|null {
        return this.isBlank() ? this.letterIfBlank : null;
    }

    public unsetLetterForBlank(): void {
        if (this.isBlank()) {
            this.letterIfBlank = null;
        }
    }
}
