System.register(["./cell-coordinates", "../enums/word-direction", "./words-formed", "../enums/board-tile-state"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var cell_coordinates_1, word_direction_1, words_formed_1, board_tile_state_1, PlayerTurn;
    return {
        setters: [
            function (cell_coordinates_1_1) {
                cell_coordinates_1 = cell_coordinates_1_1;
            },
            function (word_direction_1_1) {
                word_direction_1 = word_direction_1_1;
            },
            function (words_formed_1_1) {
                words_formed_1 = words_formed_1_1;
            },
            function (board_tile_state_1_1) {
                board_tile_state_1 = board_tile_state_1_1;
            }
        ],
        execute: function () {
            PlayerTurn = (function () {
                function PlayerTurn(startClickedCell, wordDirection, wordLookupService) {
                    this.inProgress = false; // Is someone playing piece?
                    this.isAtEdgeOfBoard = false; // Have we hit the edge of the board?
                    this.firstTileCoordinates = null;
                    this.wordDirection = null;
                    this.playedTiles = []; // An array of the tiles placed.
                    this.touchesAnotherWord = false;
                    if (startClickedCell == null || wordDirection == null) {
                        this.inProgress = false;
                        this.firstTileCoordinates = null;
                        this.wordDirection = null;
                    }
                    else {
                        this.inProgress = true;
                        this.firstTileCoordinates = startClickedCell.getCellCoordinates();
                        this.wordDirection = wordDirection;
                    }
                    this.wordsFormed = new words_formed_1.WordsFormed(wordLookupService);
                }
                PlayerTurn.prototype.getWordDirection = function () {
                    return this.wordDirection;
                };
                PlayerTurn.prototype.isInProgress = function () {
                    return this.inProgress;
                };
                PlayerTurn.prototype.canAddTiles = function () {
                    return this.inProgress && !this.isAtEdgeOfBoard;
                };
                PlayerTurn.prototype.tilesHaveBeenPlayed = function () {
                    return this.playedTiles.length > 0;
                };
                PlayerTurn.prototype.addTile = function (nextTile, board) {
                    this.playedTiles.push(nextTile);
                    // @TODO: Check whether we've now reached the edge of the board (may have to pass through previously played tiles on the board).
                    // Update the list of words formed in this go.
                    this.wordsFormed.update(board, this);
                    // Determine whether this word touches other ones.
                    this.saveTouchesOtherWords(board);
                };
                PlayerTurn.prototype.removeTile = function (board) {
                    if (this.playedTiles.length > 0) {
                        var removedTile = this.playedTiles.pop();
                        // Update the list of words formed in this go.
                        this.wordsFormed.update(board, this);
                        // Determine whether this word still touches other ones.
                        this.saveTouchesOtherWords(board);
                        return removedTile;
                    }
                    return null;
                };
                PlayerTurn.prototype.getInitialPosition = function () {
                    return this.firstTileCoordinates;
                };
                PlayerTurn.prototype.hasPlayedAllTiles = function () {
                    return (this.playedTiles.length == 7);
                };
                PlayerTurn.prototype.getWordsFormed = function () {
                    return this.wordsFormed;
                };
                PlayerTurn.prototype.saveTouchesOtherWords = function (board) {
                    this.touchesAnotherWord = this.calcTouchesOtherWords(board);
                };
                PlayerTurn.prototype.calcTouchesOtherWords = function (board) {
                    // If this is the very first go, just return true.
                    // @TODO: Don't hard-code the dimensions.
                    var centreTileState = board.getTileStateAt(new cell_coordinates_1.CellCoordinates(7, 7));
                    if (centreTileState == null || centreTileState == board_tile_state_1.BoardTileState.IN_TURN) {
                        return true;
                    }
                    // Special check: if the first letter of this word was not played in this go, then we must
                    // be touching another word!
                    var previousCellPosition = new cell_coordinates_1.CellCoordinates(this.firstTileCoordinates.getRow() - (this.wordDirection == word_direction_1.WordDirection.ACROSS ? 0 : 1), this.firstTileCoordinates.getCol() - (this.wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                    if (board.hasTileAt(previousCellPosition) && board.getTileStateAt(previousCellPosition) != board_tile_state_1.BoardTileState.IN_TURN) {
                        return true;
                    }
                    // Start at the first letter (even if played in previous turns).
                    // Step through to the end of the word. If the tile was played in a previous go or
                    // either adjacent square is populated, return true. If we get to the end of the word
                    // without finding a touching tile, return false.
                    var currentCoordinates = this.firstTileCoordinates;
                    while (board.hasTileAt(currentCoordinates)) {
                        // If the tile we're on now was laid in a previous turn, we are touching another word.
                        if (board.getTileStateAt(currentCoordinates) != board_tile_state_1.BoardTileState.IN_TURN) {
                            return true;
                        }
                        var adjoiningDeltaRow = (this.wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0);
                        var adjoiningDeltaCol = (this.wordDirection == word_direction_1.WordDirection.ACROSS ? 0 : 1);
                        var firstSide = new cell_coordinates_1.CellCoordinates(currentCoordinates.getRow() + adjoiningDeltaRow, currentCoordinates.getCol() + adjoiningDeltaCol);
                        var secondSide = new cell_coordinates_1.CellCoordinates(currentCoordinates.getRow() - adjoiningDeltaRow, currentCoordinates.getCol() - adjoiningDeltaCol);
                        if (board.hasTileAt(firstSide) || board.hasTileAt(secondSide)) {
                            // This tile is adjoining another.
                            return true;
                        }
                        currentCoordinates = new cell_coordinates_1.CellCoordinates(currentCoordinates.getRow() + (this.wordDirection == word_direction_1.WordDirection.DOWN ? 1 : 0), currentCoordinates.getCol() + (this.wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                    }
                    // Did not find any adjoining tiles.
                    return false;
                };
                PlayerTurn.prototype.isTouchingExistingTile = function () {
                    return this.touchesAnotherWord;
                };
                PlayerTurn.prototype.hasSingleLetterWord = function () {
                    return this.wordsFormed.hasSingleLetterWord();
                };
                return PlayerTurn;
            }());
            exports_1("PlayerTurn", PlayerTurn);
        }
    };
});
//# sourceMappingURL=player-turn.js.map