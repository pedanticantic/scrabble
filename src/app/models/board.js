System.register(["./cell-coordinates", "../enums/word-direction", "../enums/board-tile-state", "../enums/premium-cell-type"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var cell_coordinates_1, word_direction_1, board_tile_state_1, premium_cell_type_1, Board;
    return {
        setters: [
            function (cell_coordinates_1_1) {
                cell_coordinates_1 = cell_coordinates_1_1;
            },
            function (word_direction_1_1) {
                word_direction_1 = word_direction_1_1;
            },
            function (board_tile_state_1_1) {
                board_tile_state_1 = board_tile_state_1_1;
            },
            function (premium_cell_type_1_1) {
                premium_cell_type_1 = premium_cell_type_1_1;
            }
        ],
        execute: function () {
            Board = (function () {
                function Board(size) {
                    this.size = size;
                    this.boardRows = [];
                    this.middle = null;
                    this.middle = (size - 1) / 2;
                }
                Board.prototype.addRow = function (boardRow) {
                    this.boardRows.push(boardRow);
                };
                Board.prototype.setPremiumCell = function (premiumType, rowIndex, colIndex) {
                    this.boardRows[rowIndex].setPremiumCell(premiumType, colIndex, (rowIndex == this.middle && colIndex == this.middle));
                };
                Board.prototype.getBoardRows = function () {
                    return this.boardRows;
                };
                Board.prototype.getTileAt = function (cellCoordinates) {
                    return this.boardRows[cellCoordinates.getRow()].getTileAt(cellCoordinates);
                };
                Board.prototype.hasTileAt = function (cellCoordinates) {
                    if (!this.positionIsValid(cellCoordinates)) {
                        return false;
                    }
                    var tile = this.getTileAt(cellCoordinates);
                    return !(tile == null);
                };
                Board.prototype.getTileStateAt = function (cellCoordinates) {
                    return this.boardRows[cellCoordinates.getRow()].getTileStateAt(cellCoordinates);
                };
                Board.prototype.findInitialTile = function (fromCell, wordDirection) {
                    // If there's no tile on this square, then we have finished.
                    if (fromCell.getTile() == null) {
                        return fromCell;
                    }
                    var foundCell = false;
                    do {
                        // If we are at the edge of the board, then we have finished.
                        var cellCoordinates = fromCell.getCellCoordinates();
                        if ((cellCoordinates.getCol() == 0 && wordDirection == word_direction_1.WordDirection.ACROSS) ||
                            (cellCoordinates.getRow() == 0 && wordDirection == word_direction_1.WordDirection.DOWN)) {
                            foundCell = true;
                            break;
                        }
                        // If there is no tile to the left or above, then we have finished.
                        var previousCoords = new cell_coordinates_1.CellCoordinates(cellCoordinates.getRow() - (wordDirection == word_direction_1.WordDirection.DOWN ? 1 : 0), cellCoordinates.getCol() - (wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                        var previousTile = this.getTileAt(previousCoords);
                        if (previousTile == null) {
                            foundCell = true;
                            break;
                        }
                        fromCell = this.boardRows[previousCoords.getRow()].boardRowCells[previousCoords.getCol()];
                    } while (!foundCell);
                    return fromCell;
                };
                Board.prototype.getNextAvailableCell = function (playerTurn) {
                    var initialPosition = playerTurn.getInitialPosition();
                    var wordDirection = playerTurn.getWordDirection();
                    // Start at the initial position and move along the line until we get to an empty square.
                    while (this.hasTileAt(initialPosition)) {
                        initialPosition = new cell_coordinates_1.CellCoordinates(initialPosition.getRow() + (wordDirection == word_direction_1.WordDirection.DOWN ? 1 : 0), initialPosition.getCol() + (wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                    }
                    return this.positionIsValid(initialPosition) ?
                        this.getBoardRows()[initialPosition.getRow()].getRowCells()[initialPosition.getCol()]
                        :
                            null;
                };
                Board.prototype.playTile = function (playerTurn, playedTile) {
                    var thisCell = this.getNextAvailableCell(playerTurn);
                    if (thisCell == null) {
                        return false;
                    }
                    thisCell.setTile(playedTile);
                    return true;
                };
                Board.prototype.removeLastTile = function (playerTurn) {
                    // Start from the initial position, follow the tiles to the last letter, remembering the
                    // last cell that has a "played tile". Then clear the played tile on that cell.
                    var initialPosition = playerTurn.getInitialPosition();
                    var wordDirection = playerTurn.getWordDirection();
                    // Start at the initial position and move along the line until we get to an empty square.
                    var lastPlayedTileCell = null;
                    while (this.hasTileAt(initialPosition)) {
                        if (this.getTileStateAt(initialPosition) == board_tile_state_1.BoardTileState.IN_TURN) {
                            lastPlayedTileCell = this.getBoardRows()[initialPosition.getRow()].getRowCells()[initialPosition.getCol()];
                        }
                        initialPosition = new cell_coordinates_1.CellCoordinates(initialPosition.getRow() + (wordDirection == word_direction_1.WordDirection.DOWN ? 1 : 0), initialPosition.getCol() + (wordDirection == word_direction_1.WordDirection.ACROSS ? 1 : 0));
                    }
                    lastPlayedTileCell.clearTile();
                };
                Board.prototype.hasTileInCentre = function () {
                    return this.hasTileAt(new cell_coordinates_1.CellCoordinates(this.middle, this.middle));
                };
                Board.prototype.getMultiplierAt = function (cellCoordinate, isForTile) {
                    // If tile was not played in this go, the result is simply 1.
                    if (this.getTileStateAt(cellCoordinate) != board_tile_state_1.BoardTileState.IN_TURN) {
                        return 1;
                    }
                    // Tile was placed in this go.
                    var premiumType = this
                        .boardRows[cellCoordinate.getRow()]
                        .boardRowCells[cellCoordinate.getCol()]
                        .getPremiumType();
                    var multiplier = 1;
                    if (isForTile) {
                        switch (premiumType) {
                            case premium_cell_type_1.PremiumCellType.QLS:
                                multiplier = 4;
                                break;
                            case premium_cell_type_1.PremiumCellType.TLS:
                                multiplier = 3;
                                break;
                            case premium_cell_type_1.PremiumCellType.DLS:
                                multiplier = 2;
                                break;
                        }
                    }
                    else {
                        switch (premiumType) {
                            case premium_cell_type_1.PremiumCellType.QWS:
                                multiplier = 4;
                                break;
                            case premium_cell_type_1.PremiumCellType.TWS:
                                multiplier = 3;
                                break;
                            case premium_cell_type_1.PremiumCellType.DWS:
                                multiplier = 2;
                                break;
                        }
                    }
                    return multiplier;
                };
                Board.prototype.getTileMultiplierAt = function (cellCoordinate) {
                    return this.getMultiplierAt(cellCoordinate, true);
                };
                Board.prototype.getWordMultiplierAt = function (cellCoordinate) {
                    return this.getMultiplierAt(cellCoordinate, false);
                };
                Board.prototype.wordIsConfirmed = function () {
                    // We need to progress the states of tiles states on the board:
                    // JUST_PLAYED => CONFIRMED
                    // IN_TURN => JUST_PLAYED
                    // It's easier just to loop through every cell on the board, rather
                    // than trying to find the specific cells.
                    for (var rowIndex = 0; rowIndex < this.boardRows.length; rowIndex++) {
                        this.boardRows[rowIndex].wordIsConfirmed();
                    }
                };
                Board.prototype.hasEmptyCellFrom = function (cellCoordinates) {
                    // We'll start at the given cell, and step across and down separately, but at the same
                    // time. If we find an empty cell, we return true. If we hit both the bottom and the
                    // RHS, we return false.
                    // We need two clones of the original coordinates.
                    var acrossCoordinate = new cell_coordinates_1.CellCoordinates(cellCoordinates.getRow(), cellCoordinates.getCol());
                    var downCoordinate = new cell_coordinates_1.CellCoordinates(cellCoordinates.getRow(), cellCoordinates.getCol());
                    while (this.positionIsValid(acrossCoordinate) || this.positionIsValid(downCoordinate)) {
                        if ((this.positionIsValid(acrossCoordinate) && !this.hasTileAt(acrossCoordinate)) ||
                            (this.positionIsValid(downCoordinate) && !this.hasTileAt(downCoordinate))) {
                            // Found an empty space, either across or down from the clicked cell.
                            console.log('Found a reachable empty space');
                            return true;
                        }
                        acrossCoordinate.incrementCol();
                        downCoordinate.incrementRow();
                    }
                    // Found nothing but placed tiles.
                    console.log('No reachable empty spaces');
                    return false;
                };
                Board.prototype.positionIsValid = function (initialPosition) {
                    return (initialPosition.getRow() >= 0 &&
                        initialPosition.getRow() < this.size &&
                        initialPosition.getCol() >= 0 &&
                        initialPosition.getCol() < this.size);
                };
                Board.prototype.getMiddle = function () {
                    return this.middle;
                };
                return Board;
            }());
            exports_1("Board", Board);
        }
    };
});
//# sourceMappingURL=board.js.map