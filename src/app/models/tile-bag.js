System.register(["./../services/shuffler"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var shuffler_1, TileBag;
    return {
        setters: [
            function (shuffler_1_1) {
                shuffler_1 = shuffler_1_1;
            }
        ],
        execute: function () {
            TileBag = (function () {
                function TileBag() {
                    this.tiles = [];
                    this.shuffler = new shuffler_1.Shuffler();
                }
                TileBag.prototype.add = function (tile) {
                    this.tiles.push(tile);
                };
                TileBag.prototype.isEmpty = function () {
                    return this.tiles.length == 0;
                };
                TileBag.prototype.howManyTiles = function () {
                    return this.tiles.length;
                };
                TileBag.prototype.shuffleBag = function () {
                    this.shuffler.shuffle(this.tiles);
                };
                TileBag.prototype.takeTile = function () {
                    if (this.isEmpty()) {
                        console.log('Something is asking the tileBag for a tile, but it is empty!');
                        return null;
                    }
                    return this.tiles.pop();
                };
                TileBag.prototype.replaceTiles = function (tiles) {
                    // Add the tiles to the end of the array of tiles.
                    this.tiles = this.tiles.concat(tiles);
                    // Shuffle the bag.
                    this.shuffleBag();
                };
                return TileBag;
            }());
            exports_1("TileBag", TileBag);
        }
    };
});
//# sourceMappingURL=tile-bag.js.map