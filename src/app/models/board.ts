// Represents the board during a game.
import {BoardRow} from './board-row';
import {CellCoordinates} from "./cell-coordinates";
import {BoardRowCell} from "./board-row-cell";
import {WordDirection} from "../enums/word-direction";
import {Tile} from "./tile";
import {BoardTileState} from "../enums/board-tile-state";
import {PlayerTurn} from "./player-turn";
import {PremiumCellType} from "../enums/premium-cell-type";

export class Board {
    boardRows: BoardRow[] = [];
    private middle: number = null;

    constructor(private size: number) {
        this.middle = (size-1)/2;
    }

    public getSize() {
        return this.size;
    }

    public addRow(boardRow: BoardRow) {
        this.boardRows.push(boardRow);
    }

    public setPremiumCell(premiumType: PremiumCellType, rowIndex: number, colIndex: number) {
        this.boardRows[rowIndex].setPremiumCell(premiumType, colIndex, (rowIndex == this.middle && colIndex == this.middle));
    }

    public getBoardRows(): BoardRow[] {
        return this.boardRows;
    }

    public getTileAt(cellCoordinates: CellCoordinates): Tile {
        return this.boardRows[cellCoordinates.getRow()].getTileAt(cellCoordinates);
    }

    public hasTileAt(cellCoordinates: CellCoordinates): boolean {
        if (!this.positionIsValid(cellCoordinates)) {

            return false;
        }
        let tile = this.getTileAt(cellCoordinates);

        return !(tile == null);
    }

    public getTileStateAt(cellCoordinates: CellCoordinates): BoardTileState {
        return this.boardRows[cellCoordinates.getRow()].getTileStateAt(cellCoordinates);
    }

    public findInitialTile(fromCell: BoardRowCell, wordDirection: WordDirection): BoardRowCell {
        // If there's no tile on this square, then we have finished.
        if (fromCell.getTile() == null) {

            return fromCell;
        }
        let foundCell = false;
        do {
            // If we are at the edge of the board, then we have finished.
            let cellCoordinates = fromCell.getCellCoordinates();
            if ((cellCoordinates.getCol() == 0 && wordDirection == WordDirection.ACROSS) ||
                (cellCoordinates.getRow() == 0 && wordDirection == WordDirection.DOWN)) {
                foundCell = true;
                break;
            }
            // If there is no tile to the left or above, then we have finished.
            let previousCoords = new CellCoordinates(
                cellCoordinates.getRow() - (wordDirection == WordDirection.DOWN ? 1 : 0),
                cellCoordinates.getCol() - (wordDirection == WordDirection.ACROSS ? 1 : 0)
            );
            let previousTile = this.getTileAt(previousCoords);
            if (previousTile == null) {
                foundCell = true;
                break;
            }

            fromCell = this.boardRows[previousCoords.getRow()].boardRowCells[previousCoords.getCol()];
        } while (!foundCell);

        return fromCell;
    }

    public getNextAvailableCell(playerTurn: PlayerTurn): BoardRowCell {
        let initialPosition: CellCoordinates = playerTurn.getInitialPosition();
        let wordDirection: WordDirection = playerTurn.getWordDirection();
        // Start at the initial position and move along the line until we get to an empty square.
        while(this.hasTileAt(initialPosition)) {
            initialPosition = new CellCoordinates(
                initialPosition.getRow() + (wordDirection == WordDirection.DOWN ? 1 : 0),
                initialPosition.getCol() + (wordDirection == WordDirection.ACROSS ? 1 : 0)
            );
        }

        return this.positionIsValid(initialPosition) ?
            this.getBoardRows()[initialPosition.getRow()].getRowCells()[initialPosition.getCol()]
            :
            null;
    }

    public playTile(playerTurn: PlayerTurn, playedTile: Tile): boolean {
        let thisCell: BoardRowCell = this.getNextAvailableCell(playerTurn);
        if (thisCell == null) {

            return false;
        }
        thisCell.setTile(playedTile);

        return true;
    }

    public removeLastTile(playerTurn: PlayerTurn) {
        // Start from the initial position, follow the tiles to the last letter, remembering the
        // last cell that has a "played tile". Then clear the played tile on that cell.
        let initialPosition: CellCoordinates = playerTurn.getInitialPosition();
        let wordDirection: WordDirection = playerTurn.getWordDirection();
        // Start at the initial position and move along the line until we get to an empty square.
        let lastPlayedTileCell:BoardRowCell = null;
        while(this.hasTileAt(initialPosition)) {
            if (this.getTileStateAt(initialPosition) == BoardTileState.IN_TURN) {
                lastPlayedTileCell = this.getBoardRows()[initialPosition.getRow()].getRowCells()[initialPosition.getCol()];
            }
            initialPosition = new CellCoordinates(
                initialPosition.getRow() + (wordDirection == WordDirection.DOWN ? 1 : 0),
                initialPosition.getCol() + (wordDirection == WordDirection.ACROSS ? 1 : 0)
            );
        }

        lastPlayedTileCell.clearTile();
    }

    public hasTileInCentre(): boolean {
        return this.hasTileAt(new CellCoordinates(this.middle, this.middle));
    }

    public getMultiplierAt(cellCoordinate: CellCoordinates, isForTile: boolean): number {
        // If tile was not played in this go, the result is simply 1.
        if (this.getTileStateAt(cellCoordinate) != BoardTileState.IN_TURN) {

            return 1;
        }
        // Tile was placed in this go.
        let premiumType =
            this
                .boardRows[cellCoordinate.getRow()]
                .boardRowCells[cellCoordinate.getCol()]
                .getPremiumType();
        let multiplier = 1;
        if (isForTile) {
            switch(premiumType) {
                case PremiumCellType.QLS:
                    multiplier = 4;
                    break;
                case PremiumCellType.TLS:
                    multiplier = 3;
                    break;
                case PremiumCellType.DLS:
                    multiplier = 2;
                    break;
            }
        } else {
            switch(premiumType) {
                case PremiumCellType.QWS:
                    multiplier = 4;
                    break;
                case PremiumCellType.TWS:
                    multiplier = 3;
                    break;
                case PremiumCellType.DWS:
                    multiplier = 2;
                    break;
            }
        }

        return multiplier;
    }

    public getTileMultiplierAt(cellCoordinate: CellCoordinates): number {
        return this.getMultiplierAt(cellCoordinate, true);
    }

    public getWordMultiplierAt(cellCoordinate: CellCoordinates): number {
        return this.getMultiplierAt(cellCoordinate, false);
    }

    public wordIsConfirmed(): void {
        // We need to progress the states of tiles states on the board:
        // JUST_PLAYED => CONFIRMED
        // IN_TURN => JUST_PLAYED
        // It's easier just to loop through every cell on the board, rather
        // than trying to find the specific cells.
        for(let rowIndex = 0 ; rowIndex < this.boardRows.length ; rowIndex++) {
            this.boardRows[rowIndex].wordIsConfirmed();
        }
    }

    public hasEmptyCellFrom(cellCoordinates: CellCoordinates): boolean {
        // We'll start at the given cell, and step across and down separately, but at the same
        // time. If we find an empty cell, we return true. If we hit both the bottom and the
        // RHS, we return false.
        // We need two clones of the original coordinates.
        let acrossCoordinate = new CellCoordinates(cellCoordinates.getRow(), cellCoordinates.getCol());
        let downCoordinate = new CellCoordinates(cellCoordinates.getRow(), cellCoordinates.getCol());
        while(this.positionIsValid(acrossCoordinate) || this.positionIsValid(downCoordinate)) {
            if ((this.positionIsValid(acrossCoordinate) && !this.hasTileAt(acrossCoordinate)) ||
                (this.positionIsValid(downCoordinate) && !this.hasTileAt(downCoordinate))) {

                // Found an empty space, either across or down from the clicked cell.
                return true;
            }
            acrossCoordinate.incrementCol();
            downCoordinate.incrementRow();
        }

        // Found nothing but placed tiles.
        return false;
    }

    public positionIsValid(initialPosition: CellCoordinates): boolean {
        return (
            initialPosition.getRow() >= 0 &&
            initialPosition.getRow() < this.size &&
            initialPosition.getCol() >= 0 &&
            initialPosition.getCol() < this.size
        );
    }

    public getMiddle(): number {
        return this.middle;
    }

    public restoreState(previousState: any) {
        this.boardRows.forEach(function (boardRow: BoardRow, rowStateIndex: number) {
            boardRow.getRowCells().forEach(function (boardRowCell: BoardRowCell, cellStateIndex: number) {
                // We might need to set the tile to null, or update it (eg because of when it was placed).
                let previousTileState = previousState[rowStateIndex][cellStateIndex];
                if (previousTileState.tile == null) {
                    boardRowCell.clearTile();
                } else {
                    let previousTile = new Tile(previousTileState.tile, previousTileState.pointsValue);
                    if (previousTileState.tile == ' ') {
                        previousTile.setLetterForBlank(previousTileState.letterForBlank);
                    }
                    boardRowCell.setTile(previousTile, previousTileState.tileState);
                }
            })
        });
    }
}
